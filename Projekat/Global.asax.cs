﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Projekat
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            Baza b = new Baza();
            b.ReadXML();
            HttpContext.Current.Cache["Apartmani"] = b.UcitajApartmane();
            HttpContext.Current.Cache["Rezervacije"] = b.UcitajRezervacije();
            HttpContext.Current.Cache["Amenities"] =b.UcitajAmenities( new Dictionary<string, List<Amenities>>());
         
            HttpContext.Current.Cache["Komentari"] = b.UcitajKomentare();
        }
    }
}
