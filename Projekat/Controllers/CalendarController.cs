﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Projekat.Controllers
{
    public class CalendarController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult PrethodniM()
        {
            Dictionary<string, int> potrebno = (Dictionary<string, int>)Session["trenutnipodacimeseca"];
            int mes = 0;
            int god = potrebno["godina"];
            if (potrebno["mesec"] - 1 > 0)
            {
                mes = potrebno["mesec"] - 1;
            }
            else
            {
                mes = 12; god = potrebno["godina"] - 1;
            }
            DateTime datum = new DateTime(god, mes, 1);
            if (datum < DateTime.Now && new DateTime(god, mes, DateTime.DaysInMonth(god, mes)) < DateTime.Now)
            {

                ViewBag.mesec = DateTime.Now.ToString("MMMM");
            }
            else
            {
                int dani = DateTime.DaysInMonth(god, mes);
                potrebno["dani"] = dani;
                potrebno["godina"] = god;
                potrebno["mesec"] = mes;

                if (datum.DayOfWeek == DayOfWeek.Monday)
                    ViewBag.pocetak = 0;
                else if (datum.DayOfWeek == DayOfWeek.Tuesday)
                    ViewBag.pocetak = 1;
                else if (datum.DayOfWeek == DayOfWeek.Wednesday)
                    ViewBag.pocetak = 2;
                else if (datum.DayOfWeek == DayOfWeek.Thursday)
                    ViewBag.pocetak = 3;
                else if (datum.DayOfWeek == DayOfWeek.Friday)
                    ViewBag.pocetak = 4;
                else if (datum.DayOfWeek == DayOfWeek.Saturday)
                    ViewBag.pocetak = 5;
                else
                    ViewBag.pocetak = 6;

                Apartman ap1 = (Apartman)Session["TrenutnoGleda"];
                Korisnik k = (Korisnik)Session["Ulogovan"];
                if (k.Uloga == Uloga.Gost)
                {
                    ViewBag.DozvolaZaBook = true;
                }
                List<Rezervacija> list = (List<Rezervacija>)HttpContext.Cache["Rezervacije"];
                List<Rezervacija> list1 = new List<Rezervacija>();
                foreach (Rezervacija item in list)
                {
                    if (item.Gost == k.Korisnicko_ime)
                    {
                        list1.Add(item);
                    }
                }
                List<Rezervacija> list2 = new List<Rezervacija>();
                foreach (Rezervacija item in list1)
                {
                    if (item.apartman == ap1.Identifikacija)
                    {
                        list2.Add(item);
                    }
                }
                bool uspesno = false;
                foreach (var item in list2)
                {
                    if (item.Status == StatusRezervacije.Zavrsena || item.Status == StatusRezervacije.Odbijena)
                    {
                        uspesno = true;
                    }
                }
                if (uspesno)
                {
                    ViewBag.Dozvola = true;
                }
                List<Komentar> list5 = (List<Komentar>)HttpContext.Cache["Komentari"];
                List<Komentar> listic = new List<Komentar>();
                foreach (var item in list5)
                {
                    if (item.apartman == ap1.Identifikacija)
                    {
                        listic.Add(item);
                    }
                }
                ViewBag.Komentari = listic;//
                potrebno["prvidan"] = ViewBag.pocetak;
                Apartman ap333 = (Apartman)Session["TrenutnoGleda"];
                ViewBag.apartmanID = ap333.Identifikacija;
                ViewBag.mesec = datum.ToString("MMMM");
                Session["trenutnipodacimeseca"] = potrebno;

            }
            ViewBag.GostAPARTMAN = (Apartman)Session["TrenutnoGleda"];
            List<int> dozvoljeni = new List<int>();
            Apartman app = ViewBag.GostAPARTMAN;
            foreach (var item in app.Dostupnost_po_datumima)
            {
                if (item.Month == mes && item.Year == god)
                {
                    dozvoljeni.Add(item.Day);
                }
            }
            if (dozvoljeni.Count != 0)
                ViewBag.dozvoljeni = dozvoljeni[0] - 1;
            else
                ViewBag.dozvoljeni = 0;
            Apartman ap33 = (Apartman)Session["TrenutnoGleda"];
            ViewBag.apartmanID = ap33.Identifikacija;
            ViewBag.pocetak = potrebno["prvidan"];
            ViewBag.dana = potrebno["dani"];
            ViewBag.dozvolazarez = true;
            return View("../Index/DetaljiApartmanaGost");
        }
        [HttpPost]
        public ActionResult SledeciM()
        {
            Dictionary<string, int> potrebno = (Dictionary<string, int>)Session["trenutnipodacimeseca"];
            ViewBag.dozvolazarez = true;
            int mes = 0;
            int god = potrebno["godina"];
            if (potrebno["mesec"] + 1 <= 12)
            {
                mes = potrebno["mesec"] + 1;
            }
            else
            {
                mes = 1;
                god = potrebno["godina"] + 1;
            }
            DateTime datum = new DateTime(god, mes, 1);
            int dani = DateTime.DaysInMonth(god, mes);
            potrebno["dani"] = dani;
            potrebno["mesec"] = mes;
            potrebno["godina"] = god;

            if (datum.DayOfWeek == DayOfWeek.Monday)
                ViewBag.pocetak = 0;
            else if (datum.DayOfWeek == DayOfWeek.Tuesday)
                ViewBag.pocetak = 1;
            else if (datum.DayOfWeek == DayOfWeek.Wednesday)
                ViewBag.pocetak = 2;
            else if (datum.DayOfWeek == DayOfWeek.Thursday)
                ViewBag.pocetak = 3;
            else if (datum.DayOfWeek == DayOfWeek.Friday)
                ViewBag.pocetak = 4;
            else if (datum.DayOfWeek == DayOfWeek.Saturday)
                ViewBag.pocetak = 5;
            else
                ViewBag.pocetak = 6;
            Apartman ap1 = (Apartman)Session["TrenutnoGleda"];
            Korisnik k = (Korisnik)Session["Ulogovan"];
            if (k.Uloga == Uloga.Gost)
            {
                ViewBag.DozvolaZaBook = true;
            }
            List<Rezervacija> list = (List<Rezervacija>)HttpContext.Cache["Rezervacije"];
            List<Rezervacija> list1 = new List<Rezervacija>();
            foreach (Rezervacija item in list)
            {
                if (item.Gost == k.Korisnicko_ime)
                {
                    list1.Add(item);
                }
            }
            List<Rezervacija> list2 = new List<Rezervacija>();
            foreach (Rezervacija item in list1)
            {
                if (item.apartman == ap1.Identifikacija)
                {
                    list2.Add(item);
                }
            }
            bool uspesno = false;
            foreach (var item in list2)
            {
                if (item.Status == StatusRezervacije.Zavrsena || item.Status == StatusRezervacije.Odbijena)
                {
                    uspesno = true;
                }
            }
            if (uspesno)
            {
                ViewBag.Dozvola = true;
            }
            List<Komentar> list5 = (List<Komentar>)HttpContext.Cache["Komentari"];
            List<Komentar> listic = new List<Komentar>();
            foreach (var item in list5)
            {
                if (item.apartman == ap1.Identifikacija)
                {
                    listic.Add(item);
                }
            }
            ViewBag.Komentari = listic;//
            potrebno["prvidan"] = ViewBag.pocetak;
            ViewBag.dana = potrebno["dani"];
            Apartman ap33 = (Apartman)Session["TrenutnoGleda"];
            ViewBag.apartmanID = ap33.Identifikacija;
            ViewBag.GostAPARTMAN = (Apartman)Session["TrenutnoGleda"];
            List<int> dozvoljeni = new List<int>();
            Apartman app = ViewBag.GostAPARTMAN;
            foreach (var item in app.Dostupnost_po_datumima)
            {
                if (item.Month == mes && item.Year == god)
                {
                    dozvoljeni.Add(item.Day);
                }
            }
            if (dozvoljeni.Count != 0)
                ViewBag.dozvoljeni = dozvoljeni[0] - 1;
            else
                ViewBag.dozvoljeni = 0;
            ViewBag.mesec = datum.ToString("MMMM");
            Session["trenutnipodacimeseca"] = potrebno;
            return View("../Index/DetaljiApartmanaGost");
        }
        [HttpPost]
        public ActionResult Zabelezi(int DAN)
        {
            List<DateTime> listad = (List<DateTime>)Session["Datumi"];
            if (listad == null)
            {
                listad = new List<DateTime>();
            }
            Dictionary<string, int> potrebno = (Dictionary<string, int>)Session["trenutnipodacimeseca"];
            DateTime datum1 = new DateTime(potrebno["godina"], potrebno["mesec"], DAN);
            DateTime datum = new DateTime(potrebno["godina"], potrebno["mesec"], 1);
            int mes = potrebno["mesec"];
            int god = potrebno["godina"];
            int dani = DateTime.DaysInMonth(god, mes);

            if (datum.DayOfWeek == DayOfWeek.Monday)
                ViewBag.pocetak = 0;
            else if (datum.DayOfWeek == DayOfWeek.Tuesday)
                ViewBag.pocetak = 1;
            else if (datum.DayOfWeek == DayOfWeek.Wednesday)
                ViewBag.pocetak = 2;
            else if (datum.DayOfWeek == DayOfWeek.Thursday)
                ViewBag.pocetak = 3;
            else if (datum.DayOfWeek == DayOfWeek.Friday)
                ViewBag.pocetak = 4;
            else if (datum.DayOfWeek == DayOfWeek.Saturday)
                ViewBag.pocetak = 5;
            else
                ViewBag.pocetak = 6;
            Apartman ap1 = (Apartman)Session["TrenutnoGleda"];
            Korisnik k = (Korisnik)Session["Ulogovan"];
            if (k.Uloga == Uloga.Gost)
            {
                ViewBag.DozvolaZaBook = true;
            }
            List<Rezervacija> list = (List<Rezervacija>)HttpContext.Cache["Rezervacije"];
            List<Rezervacija> list1 = new List<Rezervacija>();
            foreach (Rezervacija item in list)
            {
                if (item.Gost == k.Korisnicko_ime)
                {
                    list1.Add(item);
                }
            }
            List<Rezervacija> list2 = new List<Rezervacija>();
            foreach (Rezervacija item in list1)
            {
                if (item.apartman == ap1.Identifikacija)
                {
                    list2.Add(item);
                }
            }
            bool uspesno = false;
            foreach (var item in list2)
            {
                if (item.Status == StatusRezervacije.Zavrsena || item.Status == StatusRezervacije.Odbijena)
                {
                    uspesno = true;
                }
            }
            if (uspesno)
            {
                ViewBag.Dozvola = true;
            }
            List<Komentar> list5 = (List<Komentar>)HttpContext.Cache["Komentari"];
            List<Komentar> listic = new List<Komentar>();
            foreach (var item in list5)
            {
                if (item.apartman == ap1.Identifikacija)
                {
                    listic.Add(item);
                }
            }
            ViewBag.Komentari = listic;
            Apartman ap33 = (Apartman)Session["TrenutnoGleda"];
            ViewBag.apartmanID = ap33.Identifikacija;
            List<int> dozvoljeni = new List<int>();
            Apartman app = (Apartman)Session["TrenutnoGleda"];
            foreach (var item in app.Dostupnost_po_datumima)
            {
                if (item.Month == mes && item.Year == god)
                {
                    dozvoljeni.Add(item.Day);
                }
            }
            if (dozvoljeni.Count != 0)
                ViewBag.dozvoljeni = dozvoljeni[0] - 1;
            else
                ViewBag.dozvoljeni = 0;

            listad.Add(datum1);
            ViewBag.dozvolazarez = true;    //
            ViewBag.dana = potrebno["dani"];
            ViewBag.pocetak = potrebno["prvidan"];
            Session["Datumi"] = listad;
            ViewBag.mesec = datum.ToString("MMMM");
            ViewBag.GostAPARTMAN = (Apartman)Session["TrenutnoGleda"];//
            return View("../Index/DetaljiApartmanaGost");
        }
        public ActionResult Rezervisi()
        {
            List<DateTime> listadatuma = (List<DateTime>)Session["Datumi"];
            Apartman a = (Apartman)Session["TrenutnoGleda"];
            Dictionary<string, Korisnik> recnik = (Dictionary<string, Korisnik>)HttpContext.Cache["Korisnici"];
            Rezervacija rez = new Rezervacija();
            rez.Gost = ((Korisnik)Session["Ulogovan"]).Korisnicko_ime;
            int broj = 0;
            List<double> dani = new List<double>();

            for (int i = 0; i < listadatuma.Count / 2; i++)
            {
                if (listadatuma[i * 2 + 1] > listadatuma[i * 2])
                    dani.Add((listadatuma[i * 2 + 1] - listadatuma[i * 2]).TotalDays);
                else
                    dani.Add((listadatuma[i * 2] - listadatuma[i * 2 + 1]).TotalDays);
            }

            foreach (var item in dani)
            {
                broj += (int)item;
            }

            if (broj > a.Datumi_za_izdavanje.Count)
            {
                Session["Datumi"] = null;
                return View("Error");
            }

            List<DateTime> dnevno = new List<DateTime>();
            List<bool> potvrda = new List<bool>();
            List<int> indexi = new List<int>();

            for (int i = 0; i < listadatuma.Count / 2; i++)
            {
                if (listadatuma[i * 2 + 1] > listadatuma[i * 2])
                {
                    DateTime pocetak = listadatuma[i * 2];
                    for (int j = 0; j < (listadatuma[i * 2 + 1] - listadatuma[i * 2]).TotalDays; j++)
                    {
                        dnevno.Add(pocetak.AddDays(j));
                        potvrda.Add(false);
                    }
                }
                else
                {
                    DateTime pocetak = listadatuma[i * 2 + 1];
                    for (int j = 0; j < (listadatuma[i * 2] - listadatuma[i * 2 + 1]).TotalDays; j++)
                    {
                        dnevno.Add(pocetak.AddDays(j));
                        potvrda.Add(false);
                    }
                }
            }

            for (int i = 0; i < dnevno.Count; i++)
            {
                for (int j = 0; j < a.Datumi_za_izdavanje.Count; j++)
                {
                    if (a.Datumi_za_izdavanje[j] == dnevno[i])
                    {
                        potvrda[i] = true;
                        indexi.Add(j);
                    }
                }
            }
            bool p = true;
            for (int i = 0; i < potvrda.Count; i++)
            {
                if (potvrda[i] == false)
                    p = false;
            }

            if (p)
            {
                for (int i = indexi.Count - 1; i >= 0; i--)
                {
                    a.Datumi_za_izdavanje.RemoveAt(indexi[i]);
                }
                List<Apartman> lista = (List<Apartman>)HttpContext.Cache["Apartmani"];
                for (int i = 0; i < lista.Count; i++)
                {
                    if (lista[i].Identifikacija == a.Identifikacija)
                    {
                        lista[i] = a;
                    }
                }
                List<Rezervacija> rezervacijas = (List<Rezervacija>)HttpContext.Cache["Rezervacije"];
                if (rezervacijas.Count == 0)
                    rez.ID = 0;
                else
                    rez.ID = rezervacijas[rezervacijas.Count - 1].ID + 1;
                rez.Nocenje = broj;
                rez.Pocetak = listadatuma[0];
                rez.UkupnaCena = broj * a.Cena_po_noci;
                rez.apartman = a.Identifikacija;
                rez.Status = StatusRezervacije.Kreirana;
                rezervacijas.Add(rez);
                if (recnik[a.Domacin].Rezervacije == null)
                    recnik[a.Domacin].Rezervacije = new List<int>();
                recnik[a.Domacin].Rezervacije.Add(rez.ID);
                HttpContext.Cache["Apartmani"] = lista;
                HttpContext.Cache["Korisnici"] = recnik;
                HttpContext.Cache["Rezervacije"] = rezervacijas;
                Baza b = new Baza();
                b.SacuvajApartmane(lista);
                b.SaveXML(recnik);
                b.SacuvajRezervacije(rezervacijas);
                ViewBag.Apartmani = lista;
                ViewBag.sadrzaj = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
                Session["Datumi"] = null;
                return View("../Index/Gost");
            }
            else
            {
                Session["Datumi"] = null;
                return View("Error");
            }
        }
    }
}