﻿using ExporterObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace Projekat.Controllers
{
    public class HostController : Controller
    {
        // GET: Host
        public ActionResult Index()
        {
            return View();
        }
        public Baza b = new Baza();
        public ActionResult AddApartment()
        {
            TimeSpan tt = new TimeSpan(14, 0, 0);
            TimeSpan tt1 = new TimeSpan(10, 0, 0);
            ViewBag.Pocetak = tt;
            ViewBag.Kraj = tt1;
            Apartman apartman = new Apartman();
            apartman.Slike = new List<string>();
            Session["Apartman"] = apartman;
            Lokacija lokacija = (Lokacija)Session["Location"];
            ViewBag.Taken = lokacija;
            ViewBag.Dozvoliti = false;
            ViewBag.Apartmani = (List<Apartman>)HttpContext.Cache["Apartmani"];
            return View("DodavanjeApartmana");
        }
        public ActionResult AddingBasic(HttpPostedFileBase file)
        {
            Apartman ap;
            if (Session["CurrentApartman"] == null)
            {
                ap = new Apartman();
            }
            else
            {
                ap = (Apartman)Session["CurrentApartman"];
            }
            ap.lokacija = new Lokacija();
            ap.lokacija.adresa = new Adresa();
            if (Request["Duzina"] != null && Request["Sirina"] != null && Request["Ulica"] != null && Request["Mesto"] != null && Request["PostanskiBroj"] != null && Request["BrojSoba"] != null && Request["BrojGostiju"] != null && Request["Tipovi"] != null && Request["Cena"] != null && Request["DatumDostupnostiOD"] != null && Request["DatumDostupnostiDO"] != null && Request["pocetakp"] != null && Request["krajp"] != null)
            {
                ap.lokacija.Duzina = float.Parse(Request["Duzina"]);
                ap.lokacija.Sirina = float.Parse(Request["Sirina"]);
                ap.lokacija.adresa.Ulica = Request["Ulica"];
                ap.lokacija.adresa.Mesto = Request["Mesto"];
                ap.lokacija.adresa.Posta = int.Parse(Request["PostanskiBroj"]);
                ap.Broj_soba = int.Parse(Request["BrojSoba"]);
                ap.Broj_gostiju = int.Parse(Request["BrojGostiju"]);
                if (Request["Tipovi"] == Tip.Apartman.ToString())
                {
                    ap.tip = Tip.Apartman;
                }
                else
                {
                    ap.tip = Tip.Soba;
                }
                ap.Cena_po_noci = float.Parse(Request["Cena"]);
                ap.Datumi_za_izdavanje = new List<DateTime>();
                ap.Dostupnost_po_datumima = new List<DateTime>();
                DateTime pp = DateTime.Parse(Request["DatumDostupnostiOD"]);
                DateTime ppp = DateTime.Parse(Request["DatumDostupnostiDO"]);
                TimeSpan t = ppp - pp;
                double tt = t.TotalDays;
                int mes = pp.Month;
                int dan = pp.Day;
                int god = pp.Year;
                for (int i = 0; i < tt; i++)
                {
                    ap.Datumi_za_izdavanje.Add(new DateTime(god, mes, dan));
                    ap.Dostupnost_po_datumima.Add(new DateTime(god, mes, dan));
                    dan++;
                    if (dan > DateTime.DaysInMonth(god, mes))
                    {
                        dan = 1;
                        if (mes == 12)
                        {
                            mes = 1;
                            god++;
                        }
                        else
                            mes++;
                    }
                }
                ap.Vreme_za_prijavu = TimeSpan.Parse(Request["pocetakp"]);
                ap.Vreme_za_odjavu = TimeSpan.Parse(Request["krajp"]);

                if (file != null)
                {
                    string picture = System.IO.Path.GetFileName(file.FileName);
                    if (ap.Slike == null)
                    {
                        ap.Slike = new List<string>();
                    }
                    if (!ap.Slike.Contains("/Uploaded/" + picture))
                    {
                        string path = System.IO.Path.Combine(Server.MapPath("~/Uploaded"), picture);
                        file.SaveAs(path);
                        ap.Slike.Add("/Uploaded/" + picture);
                        using (MemoryStream ms = new MemoryStream())
                        {
                            file.InputStream.CopyTo(ms);
                            byte[] appay = ms.GetBuffer();
                        }
                    }
                }
                Session["CurrentApartman"] = ap;

                ViewBag.Apartman = ap;
                ViewBag.Dozvoliti = true;
                ViewBag.Slike = ap.Slike;

            }
            List<Apartman> ap1 = (List<Apartman>)HttpContext.Cache["Apartmani"];
            Korisnik k = (Korisnik)Session["Ulogovan"];
            List<Apartman> la = new List<Apartman>();
            foreach (Apartman item in ap1)
            {
                if (item.Domacin == k.Korisnicko_ime)
                {
                    la.Add(item);
                }
            }
            ViewBag.Apartmani = la;
            return View("DodavanjeApartmana");
        }
        [HttpPost]
        public ActionResult SacuvajApartman()
        {
            Apartman ap = (Apartman)Session["CurrentApartman"];
            Dictionary<string, Korisnik> dictionary = (Dictionary<string, Korisnik>)HttpContext.Cache["Korisnici"];
            List<Apartman> dictionary1 = (List<Apartman>)HttpContext.Cache["Apartmani"];
            ap.Identifikacija = dictionary1.Count;
            ap.Obrisan = false;
            ap.status = Status.Neaktivan;
            ap.Domacin = ((Korisnik)Session["Ulogovan"]).Korisnicko_ime;
            dictionary[((Korisnik)Session["Ulogovan"]).Korisnicko_ime] = (Korisnik)Session["Ulogovan"];
            dictionary1.Add(ap);
            b.SacuvajApartmane(dictionary1);//
            ViewBag.Ime = ap.Domacin;
            Session["CurrentApartman"] = null;//
            HttpContext.Cache["Korisnici"] = dictionary;
            HttpContext.Cache["Apartmani"] = dictionary1;//
            List<Apartman> ap1 = (List<Apartman>)HttpContext.Cache["Apartmani"];
            Korisnik k = (Korisnik)Session["Ulogovan"];
            List<Apartman> la = new List<Apartman>();
            foreach (Apartman item in ap1)
            {
                if (item.Domacin == k.Korisnicko_ime)
                {
                    la.Add(item);
                }
            }
            ViewBag.Apartmani = la;
            Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            ViewBag.sadrzaj = lista;
            return View("../Index/Domacin");
        }
        [HttpPost]
        public ActionResult IzmeniApartman(int Id)
        {
            int index = 0;
            List<Apartman> lista = (List<Apartman>)HttpContext.Cache["Apartmani"];

            for (int i = 0; i < lista.Count; i++)
            {
                if (lista[i].Identifikacija == Id)
                    index = i;
            }
            ViewBag.sadrzaj = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            ViewBag.Apartman = lista[index];
            Session["Apartmanzaizmenu"] = lista[index];
            return View("IzmenaApartmana");
        }
        [HttpPost]
        public ActionResult SacuvajIzmene()
        {
            List<Apartman> list = (List<Apartman>)HttpContext.Cache["Apartmani"];
            Apartman ss = (Apartman)Session["Apartmanzaizmenu"];
            Apartman ap = new Apartman();
            foreach (var item in list)
            {
                if (ss.Identifikacija == item.Identifikacija)
                {
                    ap = item;
                }
            }
            if (Request["Duzina"] != null && Request["Sirina"] != null && Request["Ulica"] != null && Request["Mesto"] != null && Request["PostanskiBroj"] != null && Request["BrojSoba"] != null && Request["BrojGostiju"] != null && Request["Tipovi"] != null && Request["Cena"] != null && Request["DatumDostupnostiOD"] != null && Request["DatumDostupnostiDO"] != null && Request["pocetakp"] != null && Request["krajp"] != null)
            {
                ap.lokacija.Duzina = float.Parse(Request["Duzina"]);
                ap.lokacija.Sirina = float.Parse(Request["Sirina"]);
                ap.lokacija.adresa.Ulica = Request["Ulica"];
                ap.lokacija.adresa.Mesto = Request["Mesto"];
                ap.lokacija.adresa.Posta = int.Parse(Request["PostanskiBroj"]);
                ap.Broj_soba = int.Parse(Request["BrojSoba"]);
                ap.Broj_gostiju = int.Parse(Request["BrojGostiju"]);
                if (Request["Tipovi"] == Tip.Apartman.ToString())
                {
                    ap.tip = Tip.Apartman;
                }
                else
                {
                    ap.tip = Tip.Soba;
                }
                ap.Cena_po_noci = float.Parse(Request["Cena"]);
                ap.Datumi_za_izdavanje = new List<DateTime>();
                ap.Dostupnost_po_datumima = new List<DateTime>();
                DateTime pp = DateTime.Parse(Request["DatumDostupnostiOD"]);
                DateTime ppp = DateTime.Parse(Request["DatumDostupnostiDO"]);
                TimeSpan t = ppp - pp;
                double tt = t.TotalDays;
                int mes = pp.Month;
                int dan = pp.Day;
                int god = pp.Year;
                for (int i = 0; i < tt; i++)
                {
                    ap.Datumi_za_izdavanje.Add(new DateTime(god, mes, dan));
                    ap.Dostupnost_po_datumima.Add(new DateTime(god, mes, dan));
                    dan++;
                    if (dan > DateTime.DaysInMonth(god, mes))
                    {
                        dan = 1;
                        if (mes == 12)
                        {
                            mes = 1;
                            god++;
                        }
                        else
                            mes++;

                    }
                }
                ap.Vreme_za_prijavu = TimeSpan.Parse(Request["pocetakp"]);
                ap.Vreme_za_odjavu = TimeSpan.Parse(Request["krajp"]);
            }
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Identifikacija == ss.Identifikacija)
                {
                    list[i] = ap;
                }
            }
            b.SacuvajApartmane(list);//
            Session["Apartmanzaizmenu"] = null;//
            HttpContext.Cache["Apartmani"] = list;//
            List<Apartman> ap1 = (List<Apartman>)HttpContext.Cache["Apartmani"];
            Korisnik k = (Korisnik)Session["Ulogovan"];
            List<Apartman> la = new List<Apartman>();
            foreach (Apartman item in ap1)
            {
                if (item.Domacin == k.Korisnicko_ime)
                {
                    la.Add(item);
                }
            }
            ViewBag.Apartmani = la;
            Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            ViewBag.sadrzaj = lista;
            return View("../Index/Domacin");

        }
        [HttpPost]
        public ActionResult ObrisatiApartman(int Id)
        {
            List<Apartman> ap = (List<Apartman>)HttpContext.Cache["Apartmani"];
            foreach (Apartman item in ap)
            {
                if (item.Identifikacija == Id)
                {
                    item.Obrisan = true;
                }
            }
            b.SacuvajApartmane(ap);
            HttpContext.Cache["Apartmani"] = ap;
            ViewBag.Apartmani = (List<Apartman>)HttpContext.Cache["Apartmani"];
            Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            ViewBag.sadrzaj = lista;
            return View("../Index/Domacin");
        }
        [HttpPost]
        public ActionResult SadrzajApartmana(int Id)
        {

            Apartman ap1 = new Apartman();
            List<Apartman> ap = (List<Apartman>)HttpContext.Cache["Apartmani"];

            foreach (Apartman item in ap)
            {
                if (item.Identifikacija == Id)
                {
                    ap1 = item;
                }
            }
            Session["TrenutniApartman"] = ap1;
            return RedirectToAction("PopunjavanjeSadrzaja");
        }
        public ActionResult PopunjavanjeSadrzaja()
        {
            Dictionary<string, List<Amenities>> sad = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            ViewBag.Ameniti = sad;
            Apartman ap = (Apartman)Session["TrenutniApartman"];
            ViewBag.Sadrzaj = ap.Sadrzaj;
            return View("SadrzajDomacin");
        }
        [HttpPost]
        public ActionResult SacuvajIzmeneAmeniti()
        {
            Dictionary<string, List<Amenities>> sad = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            Apartman ap = (Apartman)Session["TrenutniApartman"];
            ap.Sadrzaj = new List<Amenities>();
            List<Apartman> ap1 = (List<Apartman>)HttpContext.Cache["Apartmani"];
            foreach (var item in sad)
            {
                foreach (var item1 in item.Value)
                {
                    if (Request[item1.Naziv] != null)
                    {
                        ap.Sadrzaj.Add(item1);
                    }
                }
            }
            for (int i = 0; i < ap1.Count; i++)
            {
                if (ap1[i].Identifikacija == ap.Identifikacija)
                {
                    ap1[i] = ap;
                }
            }
            b.SacuvajApartmane(ap1);
            HttpContext.Cache["Apartmani"] = ap1;
            Session["TrenutniApartman"] = null;
            List<Apartman> ap2 = (List<Apartman>)HttpContext.Cache["Apartmani"];
            Korisnik k = (Korisnik)Session["Ulogovan"];
            List<Apartman> la = new List<Apartman>();
            foreach (Apartman item in ap2)
            {
                if (item.Domacin == k.Korisnicko_ime)
                {
                    la.Add(item);
                }
            }
            ViewBag.Apartmani = la;
            Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            ViewBag.sadrzaj = lista;
            return View("../Index/Domacin");
        }
        [HttpPost]
        public ActionResult PretragaDugme()
        {
            List<Apartman> listaf = (List<Apartman>)HttpContext.Cache["Apartmani"];

            Korisnik k = (Korisnik)Session["Ulogovan"];
            List<Apartman> lista = new List<Apartman>();
            foreach (var item in listaf)
            {
                if (item.Domacin == k.Korisnicko_ime)
                {
                    lista.Add(item);
                }
            }
            List<Apartman> pretrazeno = null;
            if (Request["Mesto"] != null && Request["Mesto"] != "")
            {
                pretrazeno = new List<Apartman>();
                foreach (var item in lista)
                {
                    if (item.lokacija.adresa.Mesto == Request["Mesto"])
                        pretrazeno.Add(item);
                }
            }
            if (Request["OdCene"] != null && Request["OdCene"] != "")
            {
                if (pretrazeno == null)
                {
                    pretrazeno = new List<Apartman>();
                    foreach (var item in lista)
                    {
                        if (item.Broj_soba > int.Parse(Request["OdCene"]))
                            pretrazeno.Add(item);
                    }
                }
                else
                {
                    List<Apartman> temp = new List<Apartman>();

                    foreach (var item in pretrazeno)
                    {
                        if (item.Broj_soba > int.Parse(Request["OdCene"]))
                            temp.Add(item);
                    }
                    pretrazeno = temp;
                }
            }
            if (Request["DoCene"] != null && Request["DoCene"] != "")
            {
                if (pretrazeno == null)
                {
                    pretrazeno = new List<Apartman>();
                    foreach (var item in lista)
                    {
                        if (item.Broj_soba < int.Parse(Request["DoCene"]))
                            pretrazeno.Add(item);
                    }
                }
                else
                {
                    List<Apartman> temp = new List<Apartman>();
                    foreach (var item in pretrazeno)
                    {
                        if (item.Broj_soba < int.Parse(Request["DoCene"]))
                            temp.Add(item);
                    }
                    pretrazeno = temp;
                }
            }
            if (Request["OdSobe"] != null && Request["OdSobe"] != "")
            {
                if (pretrazeno == null)
                {
                    pretrazeno = new List<Apartman>();
                    foreach (var item in lista)
                    {
                        if (item.Broj_soba > int.Parse(Request["OdSobe"]))
                            pretrazeno.Add(item);
                    }
                }
                else
                {
                    List<Apartman> temp = new List<Apartman>();

                    foreach (var item in pretrazeno)
                    {
                        if (item.Broj_soba > int.Parse(Request["OdSobe"]))
                            temp.Add(item);
                    }
                    pretrazeno = temp;
                }
            }
            if (Request["DoSoba"] != null && Request["DoSoba"] != "")
            {
                if (pretrazeno == null)
                {
                    pretrazeno = new List<Apartman>();
                    foreach (var item in lista)
                    {
                        if (item.Broj_soba < int.Parse(Request["DoSoba"]))
                            pretrazeno.Add(item);
                    }
                }
                else
                {
                    List<Apartman> temp = new List<Apartman>();
                    foreach (var item in pretrazeno)
                    {
                        if (item.Broj_soba < int.Parse(Request["DoSoba"]))
                            temp.Add(item);
                    }
                    pretrazeno = temp;
                }
            }

            if (Request["BrOsoba"] != null && Request["BrOsoba"] != "")
            {
                if (pretrazeno == null)
                {
                    pretrazeno = new List<Apartman>();
                    foreach (var item in lista)
                    {
                        if (item.Broj_gostiju == int.Parse(Request["BrOsoba"]))
                            pretrazeno.Add(item);
                    }
                }
                else
                {
                    List<Apartman> temp = new List<Apartman>();
                    foreach (var item in pretrazeno)
                    {
                        if (item.Broj_gostiju == int.Parse(Request["BrOsoba"]))
                            temp.Add(item);
                    }
                    pretrazeno = temp;
                }
            }
            if (Request["sort"] != null)
            {
                if (pretrazeno == null)
                {
                    if (Request["sort"] == "yes")
                    {
                        for (int i = 0; i < lista.Count - 1; i++)
                        {
                            for (int j = i + 1; j < lista.Count; j++)
                            {
                                if (lista[i].Cena_po_noci > lista[j].Cena_po_noci)
                                {
                                    Apartman temp = lista[i];
                                    lista[i] = lista[j];
                                    lista[j] = temp;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < lista.Count - 1; i++)
                        {
                            for (int j = i + 1; j < lista.Count; j++)
                            {
                                if (lista[i].Cena_po_noci < lista[j].Cena_po_noci)
                                {
                                    Apartman temp = lista[i];
                                    lista[i] = lista[j];
                                    lista[j] = temp;
                                }
                            }
                        }
                    }
                    pretrazeno = lista;
                }
                else
                {
                    if (Request["sort"] == "yes")
                    {
                        for (int i = 0; i < pretrazeno.Count - 1; i++)
                        {
                            for (int j = i + 1; j < pretrazeno.Count; j++)
                            {
                                if (pretrazeno[i].Cena_po_noci > pretrazeno[j].Cena_po_noci)
                                {
                                    Apartman temp = pretrazeno[i];
                                    pretrazeno[i] = pretrazeno[j];
                                    pretrazeno[j] = temp;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < pretrazeno.Count - 1; i++)
                        {
                            for (int j = i + 1; j < pretrazeno.Count; j++)
                            {
                                if (pretrazeno[i].Cena_po_noci < pretrazeno[j].Cena_po_noci)
                                {
                                    Apartman temp = pretrazeno[i];
                                    pretrazeno[i] = pretrazeno[j];
                                    pretrazeno[j] = temp;
                                }
                            }
                        }
                    }
                }
            }
            Session["Pretragal"] = null;
            Dictionary<string, List<Amenities>> sad = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            if (pretrazeno == null)
            {
                pretrazeno = lista;
            }
            ViewBag.Apartmani = pretrazeno;
            ViewBag.sadrzaj = sad;
            return View("../Index/Domacin");
        }
        [HttpPost]
        public ActionResult ListaRezervacije()
        {
            List<Rezervacija> listar = (List<Rezervacija>)HttpContext.Cache["Rezervacije"];
            List<Rezervacija> listar1 = new List<Rezervacija>();
            List<Apartman> ap2 = (List<Apartman>)HttpContext.Cache["Apartmani"];
            Korisnik k = (Korisnik)Session["Ulogovan"];
            List<Apartman> ap3 = new List<Apartman>();
            foreach (var item in ap2)
            {
                if (item.Domacin == k.Korisnicko_ime)
                {
                    ap3.Add(item);
                }
            }
            List<Rezervacija> lista = new List<Rezervacija>();
            foreach (var item in listar)
            {
                foreach (var item1 in ap3)
                {
                    if (item.apartman == item1.Identifikacija)
                    {
                        listar1.Add(item);
                    }
                }
            }
            listar = listar1;
            List<int> opcije = new List<int>();
            foreach (var item in listar)
            {
                if (item.Status == StatusRezervacije.Kreirana)
                {
                    opcije.Add(1);
                }
                else if (item.Status == StatusRezervacije.Prihvacena)
                {
                    if (item.Pocetak.AddDays(item.Nocenje) < DateTime.Now)
                    {
                        opcije.Add(5);
                    }
                    else
                    {
                        opcije.Add(2);
                    }
                }
                else if (item.Status == StatusRezervacije.Odbijena)
                {
                    opcije.Add(3);
                }
                else if (item.Status == StatusRezervacije.Odustanak)
                {
                    opcije.Add(4);
                }
                else
                {
                    opcije.Add(6);
                }
            }
            ViewBag.rezervacija = listar;
            ViewBag.opcija = opcije;
            return View("ListaRezervacije");
        }

        public ActionResult PrStatusaRezervacije()
        {
            int id = int.Parse(Request["id"]);
            List<Rezervacija> listar = (List<Rezervacija>)HttpContext.Cache["Rezervacije"];
            List<Rezervacija> listarrr = new List<Rezervacija>();
            Korisnik k = (Korisnik)Session["Ulogovan"];
            List<Apartman> listaA = (List<Apartman>)HttpContext.Cache["Apartmani"];
            List<Apartman> listapp = new List<Apartman>();
            foreach (var item in listaA)
            {
                if (item.Domacin == k.Korisnicko_ime)
                    listapp.Add(item);
            }

            int ident = -1;
            for (int i = 0; i < listar.Count; i++)
            {
                if (listar[i].ID == id)
                {
                    ident = listar[i].ID;
                    if (StatusRezervacije.Prihvacena.ToString() == Request["vrednost"])
                    {
                        listar[i].Status = StatusRezervacije.Prihvacena;
                    }
                    else if (Request["vrednost"] == StatusRezervacije.Odbijena.ToString())
                    {
                        listar[i].Status = StatusRezervacije.Odbijena;
                    }
                    else if (Request["vrednost"] == StatusRezervacije.Zavrsena.ToString())
                    {
                        listar[i].Status = StatusRezervacije.Zavrsena;
                    }
                }
            }
            b.SacuvajApartmane(listaA);
            b.SacuvajRezervacije(listar);
            foreach (var item in listar)
            {
                foreach (var item1 in listapp)
                {
                    if (item1.Identifikacija == item.apartman)
                        listarrr.Add(item);
                }
            }

            List<int> opcije = new List<int>();
            foreach (var item in listarrr)
            {
                if (item.Status == StatusRezervacije.Kreirana)
                {
                    opcije.Add(1);
                }
                else if (item.Status == StatusRezervacije.Prihvacena)
                {
                    if (item.Pocetak.AddDays(item.Nocenje) < DateTime.Now)
                    {
                        opcije.Add(5);
                    }
                    else
                    {
                        opcije.Add(2);
                    }
                }
                else if (item.Status == StatusRezervacije.Odbijena)
                {
                    opcije.Add(3);

                }
                else if (item.Status == StatusRezervacije.Odustanak)
                {
                    opcije.Add(4);
                }
                else if (item.Status == StatusRezervacije.Zavrsena)
                {
                    opcije.Add(6);
                }

            }
            ViewBag.rezervacija = listarrr;
            ViewBag.opcija = opcije;
            return View("ListaRezervacije");
        }
        public ActionResult FilterRezervacija()
        {
            List<Rezervacija> list = (List<Rezervacija>)HttpContext.Cache["Rezervacije"];
            List<Rezervacija> list1 = new List<Rezervacija>();
            Korisnik korisnik = (Korisnik)Session["Ulogovan"];

            foreach (var item in list)
            {
                if (item.Status.ToString() == Request["status"])
                {
                    list1.Add(item);
                }
            }
            List<int> opcije = new List<int>();
            foreach (var item in list1)
            {
                if (item.Status == StatusRezervacije.Kreirana)
                {
                    opcije.Add(1);
                }
                else if (item.Status == StatusRezervacije.Prihvacena)
                {
                    if (item.Pocetak.AddDays(item.Nocenje) < DateTime.Now)
                    {
                        opcije.Add(5);
                    }
                    else
                    {
                        opcije.Add(2);
                    }
                }
                else if (item.Status == StatusRezervacije.Odbijena)
                {
                    opcije.Add(3);
                }
                else if (item.Status == StatusRezervacije.Odustanak)
                {
                    opcije.Add(4);
                }
                else
                {
                    opcije.Add(6);
                }
            }
            ViewBag.rezervacija = list1;
            ViewBag.opcija = opcije;
            return View("ListaRezervacije");
        }
        public ActionResult VratiNaDomacin()
        {
            Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            ViewBag.sadrzaj = lista;
            ViewBag.Apartmani = (List<Apartman>)HttpContext.Cache["Apartmani"];
            return View("../Index/Domacin");
        }
        public ActionResult VratiNaPocetnuDomaci()
        {
            Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            ViewBag.sadrzaj = lista;
            ViewBag.Apartmani = (List<Apartman>)HttpContext.Cache["Apartmani"];
            return View("../Index/Domacin");
        }
        public ActionResult VratiUDomacina()
        {
            Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            ViewBag.sadrzaj = lista;
            ViewBag.Apartmani = (List<Apartman>)HttpContext.Cache["Apartmani"];
            return View("../Index/Domacin");
        }

        [HttpPost]
        public ActionResult PrikaziSveKomentareDomacin(int Id)
        {
            List<Komentar> list = (List<Komentar>)HttpContext.Cache["Komentari"];
            List<Komentar> listTemp = new List<Komentar>();
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Id == Id)
                {
                    listTemp.Add(list[i]);
                }
            }
            ViewBag.Komentar = listTemp;
            return View("KomentarDomacin");
        }
        [HttpPost]
        public ActionResult KomentarPrikaz(int Id)
        {
            List<Komentar> list = (List<Komentar>)HttpContext.Cache["Komentari"];
            int number = -1;
            for (int i = 0; i < list.Count; i++)
            {
                if (Id == list[i].Id)
                {
                    list[i].Prikaz = true;
                    number = list[i].Id;
                }
            }
            HttpContext.Cache["Komentari"] = list;
            b.SacuvajKomentare(list);
            List<Komentar> listTemp = new List<Komentar>();
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Id == number)
                {
                    listTemp.Add(list[i]);
                }
            }
            ViewBag.Komentar = listTemp;
            return View("KomentarDomacin");
        }
        [HttpPost]
        public ActionResult PretragaRezervacijeDomacin(string Tekst)
        {
            List<Rezervacija> listar = (List<Rezervacija>)HttpContext.Cache["Rezervacije"];
            Korisnik k = (Korisnik)Session["Ulogovan"];
           
            List<Rezervacija> rezervacija = new List<Rezervacija>();
            List<Rezervacija> rezervacija1 = new List<Rezervacija>();
            foreach (Rezervacija item in listar)
            {
                if (item.Gost == Tekst )
                {
                    rezervacija.Add(item);
                }
            }
            foreach (var item in rezervacija)
            {
                foreach (var item1 in k.Apartmani)
                {
                    if(item.apartman==item1)
                    {
                        rezervacija1.Add(item);
                    }
                }
            }

            List<int> opcije = new List<int>();
            foreach (var item in rezervacija1)
            {
                if (item.Status == StatusRezervacije.Kreirana)
                {
                    opcije.Add(1);
                }
                else if (item.Status == StatusRezervacije.Prihvacena)
                {
                    if (item.Pocetak.AddDays(item.Nocenje) < DateTime.Now)
                    {
                        opcije.Add(5);
                    }
                    else
                    {
                        opcije.Add(2);
                    }
                }
                else if (item.Status == StatusRezervacije.Odbijena)
                {
                    opcije.Add(3);

                }
                else if (item.Status == StatusRezervacije.Odustanak)
                {
                    opcije.Add(4);
                }
                else if (item.Status == StatusRezervacije.Zavrsena)
                {
                    opcije.Add(6);
                }

            }

            ViewBag.rezervacija = rezervacija1;
            ViewBag.opcija = opcije;
            return View("ListaRezervacije");
        }

    }
}