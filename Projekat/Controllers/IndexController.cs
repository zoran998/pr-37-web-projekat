﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ExporterObjects;
using System.Xml;
using System.IO;

namespace Projekat.Controllers
{
    public class IndexController : Controller
    {
        public static int dodaj;
        public Baza b = new Baza();
        // GET: Index
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Pocetna()
        {
            return View();
        }
        public ActionResult Register()
        {
            Korisnik korisnik = (Korisnik)Session["Ulogovan"];
            if (korisnik != null && (korisnik.Uloga == Uloga.Domacin || korisnik.Uloga == Uloga.Gost))
            {
                ViewBag.Izlogovati = "Accept";
                return View("Pocetna");
            }
            return View("Register");
        }
        [HttpPost]
        public ActionResult Login(string Korisnicko_ime, string Lozinka)
        {
            if (Session["Ulogovan"] != null)
            {
                ViewBag.Izlogovati = "Accept";
                return View("Pocetna");
            }
            Dictionary<string, Korisnik> Recnik = (Dictionary<string, Korisnik>)HttpContext.Cache["Korisnici"];
            foreach (var item in Recnik.Values)
            {
                if (item.Korisnicko_ime == Korisnicko_ime && item.Lozinka == Lozinka)
                {
                    Session["Ulogovan"] = item;
                    ViewBag.Ime = Korisnicko_ime;
                    if (item.Uloga == Uloga.Administrator)
                    {
                        ViewBag.Pogresno = null;
                        Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
                        ViewBag.sadrzaj = lista;
                        return View("Administrator");
                    }
                    else if (item.Uloga == Uloga.Domacin)
                    {
                        ViewBag.Pogresno = null;
                        List<Apartman> ap = (List<Apartman>)HttpContext.Cache["Apartmani"];
                        Korisnik k = (Korisnik)Session["Ulogovan"];
                        List<Apartman> la = new List<Apartman>();
                        foreach (Apartman item1 in ap)
                        {
                            if (item1.Domacin == k.Korisnicko_ime)
                            {
                                la.Add(item1);
                            }
                        }
                        ViewBag.Apartmani = la;
                        ViewBag.sadrzaj = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
                        return View("Domacin");
                    }
                    else
                    {
                        ViewBag.Pogresno = null;
                        ViewBag.Apartmani = (List<Apartman>)HttpContext.Cache["Apartmani"];
                        Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
                        ViewBag.sadrzaj = lista;
                        return View("Gost");
                    }
                }
            }
            ViewBag.Pogresno = "Invalid username/password!";
            return View("Pocetna");
        }
        [HttpPost]
        public ActionResult LogOut()
        {
            Session["Ulogovan"] = null;
            Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            ViewBag.sadrzaj = lista;
            return View("Pocetna");
        }
        [HttpPost]
        public ActionResult Registruj(Korisnik korisniK)
        {
            Korisnik korisnik = (Korisnik)Session["Ulogovan"];
            if (korisnik != null && korisnik.Uloga == Uloga.Administrator && (string)Session["Adminski"] == "Accept")
            {
                korisniK.Uloga = Uloga.Domacin;
            }
            else
            {
                korisniK.Uloga = Uloga.Gost;
            }
            Dictionary<string, Korisnik> Recnik = (Dictionary<string, Korisnik>)HttpContext.Cache["Korisnici"];
            if (Recnik.ContainsKey(korisniK.Korisnicko_ime))
            {
                ViewBag.Postoji = "Accept";
                ViewBag.Ime = korisniK.Korisnicko_ime;
                return View("Register");
            }
            Recnik.Add(korisniK.Korisnicko_ime, korisniK);
            HttpContext.Cache["Korisnici"] = Recnik;
            ViewBag.Registering = "Login to continue!";
            if (korisnik != null && korisnik.Uloga == Uloga.Administrator && (string)Session["Adminski"] == "Accept")
            {
                Korisnik korisnicki = (Korisnik)Session["Ulogovan"];
                ViewBag.Ime = korisnicki.Korisnicko_ime;
                ViewBag.Ispravno = "Accept";
                Session["Adminski"] = null;
                b.SaveXML(Recnik);
                Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
                ViewBag.sadrzaj = lista;
                return View("Administrator");
            }
            else
            {
                b.SaveXML(Recnik);
                return View("Pocetna");
            }
        }

        
        public List<Korisnik> ListaKorisnika()
        {
            List<Korisnik> korisnici = new List<Korisnik>();
            Dictionary<string, Korisnik> Recnik = (Dictionary<string, Korisnik>)HttpContext.Cache["Korisnici"];
            foreach (var item in Recnik.Values)
            {
                korisnici.Add(item);
            }
            return korisnici;
        }
        [HttpPost]
        public ActionResult AdminOpcije(string Submit)
        {
            if (Submit == "Add Host")
            {
                Session["Adminski"] = "Accept";
                ViewBag.Adminski = "Accept";
                return View("Register");
            }
            else if (Submit == "List of users")
            {
                List<Korisnik> korisnici = ListaKorisnika();
                ViewBag.ListKorisnika = korisnici;
                Korisnik korisnik = (Korisnik)Session["Ulogovan"];
                ViewBag.Ime = korisnik.Korisnicko_ime;
                Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
                ViewBag.sadrzaj = lista;
                return View("Administrator");
            }
            return View("Greska");
        }
        public ActionResult VracanjeUAdmin()
        {
            Korisnik korisnik = (Korisnik)Session["Ulogovan"];
            ViewBag.Ime = korisnik.Korisnicko_ime;
            ViewBag.Promena = null;
            Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            ViewBag.sadrzaj = lista;
            return View("Administrator");
        }
        public ActionResult VracanjeUGost()
        {
            Korisnik korisnik = (Korisnik)Session["Ulogovan"];
            ViewBag.Ime = korisnik.Korisnicko_ime;
            ViewBag.Promena = null;
            ViewBag.Apartmani = (List<Apartman>)HttpContext.Cache["Apartmani"];
            Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            ViewBag.sadrzaj = lista;
            return View("Gost");
        }
        public ActionResult VracanjeUDomacin()
        {
            Korisnik korisnik = (Korisnik)Session["Ulogovan"];
            ViewBag.Ime = korisnik.Korisnicko_ime;
            ViewBag.Promena = null;
            Korisnik k = (Korisnik)Session["Ulogovan"];
            List<Apartman> ap = (List<Apartman>)HttpContext.Cache["Apartmani"];
            List<Apartman> la = new List<Apartman>();
            foreach (Apartman item in ap)
            {
                if (item.Domacin == k.Korisnicko_ime)
                {
                    la.Add(item);
                }
            }
            ViewBag.Apartmani = la;
            Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            ViewBag.sadrzaj = lista;
            return View("Domacin");
        }

        public ActionResult FilterRezervacija()
        {
            List<Rezervacija> list = (List<Rezervacija>)HttpContext.Cache["Rezervacije"];
            List<Rezervacija> list1 = new List<Rezervacija>();
            Korisnik korisnik = (Korisnik)Session["Ulogovan"];

            foreach (var item in list)
            {
                if(item.Status.ToString()==Request["status"])
                {
                    list1.Add(item);
                }
            }
            List<int> opcije = new List<int>();
            foreach (var item in list1)
            {
                if (item.Status == StatusRezervacije.Kreirana)
                {
                    opcije.Add(1);
                }
                else if (item.Status == StatusRezervacije.Prihvacena)
                {
                    if (item.Pocetak.AddDays(item.Nocenje) < DateTime.Now)
                    {
                        opcije.Add(5);
                    }
                    else
                    {
                        opcije.Add(2);
                    }
                }
                else if (item.Status == StatusRezervacije.Odbijena)
                {
                    opcije.Add(3);
                }
                else if (item.Status == StatusRezervacije.Odustanak)
                {
                    opcije.Add(4);
                }
                else
                {
                    opcije.Add(6);
                }
            }
            ViewBag.rezervacija = list1;
            ViewBag.opcija = opcije;
            return View("ListaRezervacije");
        }
        public ActionResult Change(string opcija)
        {
            Korisnik korisnik = (Korisnik)Session["Ulogovan"];
            ViewBag.Ime = korisnik.Korisnicko_ime;
            ViewBag.Change = "Ready";
            if (opcija == "Admin")
            {
                Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
                ViewBag.sadrzaj = lista;
                return View("Administrator");
            }
            else if (opcija == "Domacin")
            {
                List<Apartman> ap = (List<Apartman>)HttpContext.Cache["Apartmani"];
                Korisnik k = (Korisnik)Session["Ulogovan"];
                List<Apartman> la = new List<Apartman>();
                foreach (Apartman item in ap)
                {
                    if (item.Domacin == k.Korisnicko_ime)
                    {
                        la.Add(item);
                    }
                }
                Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
                ViewBag.sadrzaj = lista;
                ViewBag.Apartmani = la;
                return View("Domacin");
            }
            else
            {
                ViewBag.Apartmani = (List<Apartman>)HttpContext.Cache["Apartmani"];
                Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
                ViewBag.sadrzaj = lista;
                return View("Gost");
            }
        }
        [HttpPost]
        public ActionResult DoWork(string option)
        {
            Korisnik korisnik = (Korisnik)Session["Ulogovan"];
            ViewBag.Ime = korisnik.Korisnicko_ime;
            ViewBag.User = Session["Ulogovan"];
            if (option == "Admin")
            {
                Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
                ViewBag.sadrzaj = lista;
                return View("Administrator");
            }
            else if (option == "Domacin")
            {
                List<Apartman> ap = (List<Apartman>)HttpContext.Cache["Apartmani"];
                Korisnik k = (Korisnik)Session["Ulogovan"];
                List<Apartman> la = new List<Apartman>();
                foreach (Apartman item in ap)
                {
                    if (item.Domacin == k.Korisnicko_ime)
                    {
                        la.Add(item);
                    }
                }
                ViewBag.Apartmani = la;
                Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
                ViewBag.sadrzaj = lista;
                return View("Domacin");
            }
            else
            {
                ViewBag.Apartmani = (List<Apartman>)HttpContext.Cache["Apartmani"];
                Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
                ViewBag.sadrzaj = lista;
                return View("Gost");
            }
        }
        [HttpPost]
        public ActionResult Changes(Korisnik korisnik)
        {
            Dictionary<string, Korisnik> Recnik = (Dictionary<string, Korisnik>)HttpContext.Cache["Korisnici"];
            Korisnik korisniK = (Korisnik)Session["Ulogovan"];
            string username = korisniK.Korisnicko_ime;

            if (korisniK.Korisnicko_ime == korisnik.Korisnicko_ime)
            {
                Recnik[korisniK.Korisnicko_ime] = korisnik;
                Recnik[korisniK.Korisnicko_ime].Uloga = korisniK.Uloga;
                Session["Ulogovan"] = korisnik;
            }
            else if (Recnik.ContainsKey(korisnik.Korisnicko_ime) && korisniK.Korisnicko_ime != korisnik.Korisnicko_ime)
            {
                ViewBag.Exists = "Accept";
                ViewBag.User = Session["Ulogovan"];
                Korisnik KorisniK = (Korisnik)Session["Ulogovan"];
                ViewBag.Ime = KorisniK.Korisnicko_ime;

                if (KorisniK.Uloga == Uloga.Administrator)
                {
                    Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
                    ViewBag.sadrzaj = lista;
                    return View("Administrator");
                }
                else if (KorisniK.Uloga == Uloga.Domacin)
                {
                    ViewBag.Apartmani = (List<Apartman>)HttpContext.Cache["Apartmani"];
                    Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
                    ViewBag.sadrzaj = lista;
                    return View("Domacin");
                }
                else
                {
                    ViewBag.Apartmani = (List<Apartman>)HttpContext.Cache["Apartmani"];
                    Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
                    ViewBag.sadrzaj = lista;
                    return View("Gost");
                }
            }
            if (Recnik.ContainsKey(korisniK.Korisnicko_ime) && korisniK.Korisnicko_ime != korisnik.Korisnicko_ime)
            {
                Recnik.Remove(korisniK.Korisnicko_ime);
                korisnik.Uloga = korisniK.Uloga;
                Recnik.Add(korisnik.Korisnicko_ime, korisnik);
                Session["Ulogovan"] = korisnik;
            }
            HttpContext.Cache["Korisnici"] = Recnik;
            Korisnik KOrisniK = (Korisnik)Session["Ulogovan"];
            ViewBag.Ime = KOrisniK.Korisnicko_ime;
            b.SaveXML(Recnik);
            List<Apartman> ap3 = (List<Apartman>)HttpContext.Cache["Apartmani"];
            foreach (var item in ap3)
            {
                if (item.Domacin == username)
                {
                    item.Domacin = korisnik.Korisnicko_ime;
                }
            }
            b.SacuvajApartmane(ap3);
            if (KOrisniK.Uloga == Uloga.Administrator)
            {
                Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
                ViewBag.sadrzaj = lista;
                return View("Administrator");
            }
            else if (KOrisniK.Uloga == Uloga.Domacin)
            {
                List<Apartman> ap = (List<Apartman>)HttpContext.Cache["Apartmani"];/////////////
                Korisnik k = (Korisnik)Session["Ulogovan"];
                List<Apartman> la = new List<Apartman>();
                foreach (Apartman item in ap)
                {
                    if (item.Domacin == k.Korisnicko_ime)
                    {
                        la.Add(item);
                    }
                }
                ViewBag.Apartmani = la;
                Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
                ViewBag.sadrzaj = lista;
                return View("Domacin");
            }
            else
            {
                ViewBag.Apartmani = (List<Apartman>)HttpContext.Cache["Apartmani"];
                Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
                ViewBag.sadrzaj = lista;
                return View("Gost");
            }
        }

        public ActionResult ListaApartmana()
        {
            ViewBag.Apartmani = (List<Apartman>)HttpContext.Cache["Apartmani"];
            Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            ViewBag.sadrzaj = lista;
            return View("ListaApartmana");
        }
        public ActionResult Apartmani()
        {
            ViewBag.Apartmani = (List<Apartman>)HttpContext.Cache["Apartmani"];
            Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            ViewBag.sadrzaj = lista;
            return View("Apartmani");
        }
        [HttpPost]
        public ActionResult ListaRezervacijeGost()
        {
            List<Rezervacija> listar = (List<Rezervacija>)HttpContext.Cache["Rezervacije"];

            List<int> opcije = new List<int>();
            foreach (var item in listar)
            {

                if (item.Status == StatusRezervacije.Kreirana)
                {
                    opcije.Add(1);
                }
                else if (item.Status == StatusRezervacije.Prihvacena)
                {
                    if (item.Pocetak.AddDays(item.Nocenje) < DateTime.Now)
                    {
                        opcije.Add(5);
                    }
                    else
                    {
                        opcije.Add(2);
                    }
                }
                else if (item.Status == StatusRezervacije.Odbijena)
                {
                    opcije.Add(3);
                }
                else if (item.Status == StatusRezervacije.Odustanak)
                {
                    opcije.Add(4);
                }
                else
                {
                    opcije.Add(5);
                }
            }
            ViewBag.rezervacija = listar;
            ViewBag.opcija = opcije;
            return View("ListaRezervacijeGost");
        }
        public ActionResult ListaRezervacije()
        {
            List<Rezervacija> listar = (List<Rezervacija>)HttpContext.Cache["Rezervacije"];
            List<int> opcije = new List<int>();
            foreach (var item in listar)
            {
                if (item.Status == StatusRezervacije.Kreirana)
                {
                    opcije.Add(1);
                }
                else if (item.Status == StatusRezervacije.Prihvacena)
                {
                    if (item.Pocetak.AddDays(item.Nocenje) < DateTime.Now)
                    {
                        opcije.Add(5);
                    }
                    else
                    {
                        opcije.Add(2);
                    }
                }
                else if (item.Status == StatusRezervacije.Odbijena)
                {
                    opcije.Add(3);
                }
                else if (item.Status == StatusRezervacije.Odustanak)
                {
                    opcije.Add(4);
                }
                else
                {
                    opcije.Add(6);
                }
            }
            ViewBag.rezervacija = listar;
            ViewBag.opcija = opcije;
            return View("ListaRezervacije");
        }
        public ActionResult ListaAmenities()
        {
            Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            ViewBag.liste = lista;
            return View("ListaAmenities");
        }
        [HttpPost]
        public ActionResult PromenaStatus(int Id)
        {
            List<Apartman> ap = (List<Apartman>)HttpContext.Cache["Apartmani"];
            foreach (Apartman item in ap)
            {
                if (item.Identifikacija == Id)
                {
                    if (item.status == Status.Aktivan)
                    {
                        item.status = Status.Neaktivan;
                    }
                    else
                    {
                        item.status = Status.Aktivan;
                    }
                }
            }
            b.SacuvajApartmane(ap);
            HttpContext.Cache["Apartmani"] = ap;
            ViewBag.Apartmani = (List<Apartman>)HttpContext.Cache["Apartmani"];
            Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            ViewBag.sadrzaj = lista;
            return View("ListaApartmana");
        }
        public ActionResult VratiUAdmin()
        {
            Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            ViewBag.sadrzaj = lista;
            return View("Administrator");
        }
        public ActionResult VratiUGostIzKomentara()
        {
            Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            ViewBag.sadrzaj = lista;
            ViewBag.Apartmani = (List<Apartman>)HttpContext.Cache["Apartmani"];
            List<Komentar> list = (List<Komentar>)HttpContext.Cache["Komentari"];
            ViewBag.Komentar = list;
            return View("Gost");
        }
        public ActionResult VratiNaPocetnu()
        {
            Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            ViewBag.sadrzaj = lista;
            ViewBag.Apartmani = (List<Apartman>)HttpContext.Cache["Apartmani"];
            return View("Pocetna");
        }
        [HttpPost]
        public ActionResult DodavanjeSadrzaja(string Naziv, string Opis, string Kljuc)
        {
            Dictionary<string, List<Amenities>> sad = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            bool pronaso = false;
            foreach (var item in sad[Kljuc])
            {
                if (item.Naziv == Naziv)
                    pronaso = true;
            }
            if (!pronaso)
            {
                if (Opis != "")
                {
                    sad[Kljuc].Add(new Amenities() { Naziv = Naziv, Id = sad[Kljuc].Count, Opis = Opis });

                }
                else
                    sad[Kljuc].Add(new Amenities(Naziv, sad[Kljuc].Count));
            }
            b.SacuvajAmenities(sad);
            HttpContext.Cache["Amenities"] = sad;
            return RedirectToAction("MenjanjeSadrzaja");
        }
        [HttpPost]
        public ActionResult PromeniAmeniti(string Kljuc)
        {
            Dictionary<string, List<Amenities>> sad = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            ViewBag.promena = sad[Kljuc];
            ViewBag.kljuc = Kljuc;
            return View("ListaAmenities");
        }
        [HttpPost]
        public ActionResult SacuvajPromenuAmeniti()
        {
            Dictionary<string, List<Amenities>> sad = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            List<Amenities> lista = new List<Amenities>();
            for (int i = 0; i < sad[Request["Kljuc"]].Count; i++)
            {
                Amenities a = new Amenities();
                a.Naziv = Request["Naziv_" + i];
                a.Id = i;
                if (Request["Opis_" + i] != "")
                {
                    a.Opis = Request["Opis_" + i];
                }
                lista.Add(a);
            }
            sad[Request["Kljuc"]] = lista;
            HttpContext.Cache["Amenities"] = sad;
            b.SacuvajAmenities(sad);
            return RedirectToAction("MenjanjeSadrzaja");
        }
        [HttpPost]
        public ActionResult ObrisiPojedinacno(string Naziv)
        {

            Dictionary<string, List<Amenities>> sad = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            foreach (var item in sad.Values)
            {
                int id = -1;
                int i;
                for (i = 0; i < item.Count; i++)
                {
                    if (item[i].Naziv == Naziv)
                    {
                        id = i;
                    }
                }
                if (id != -1)
                {
                    item.RemoveAt(id);
                    for (i = id; i < item.Count; i++)
                    {
                        item[i].Id = item[i].Id - 1;
                    }
                }
            }
            HttpContext.Cache["Amenities"] = sad;
            b.SacuvajAmenities(sad);
            return RedirectToAction("MenjanjeSadrzaja");
        }
        [HttpPost]
        public ActionResult ObrisiApartman(int Id)
        {
            List<Apartman> ap = (List<Apartman>)HttpContext.Cache["Apartmani"];
            foreach (Apartman item in ap)
            {
                if (item.Identifikacija == Id)
                {
                    item.Obrisan = true;
                }
            }
            b.SacuvajApartmane(ap);
            HttpContext.Cache["Apartmani"] = ap;
            ViewBag.Apartmani = (List<Apartman>)HttpContext.Cache["Apartmani"];
            Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            ViewBag.sadrzaj = lista;
            return View("ListaApartmana");
        }
        public ActionResult MenjanjeSadrzaja()
        {
            Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            ViewBag.liste = lista;
            return View("ListaAmenities");
        }

        public ActionResult Filter(string Naziv)
        {
            Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            List<Apartman> ap = (List<Apartman>)HttpContext.Cache["Apartmani"];
            List<Apartman> ap1 = new List<Apartman>();
            List<Amenities> am = new List<Amenities>();
            if ((List<Amenities>)Session["AmenitiZaPretragu"] != null)
            {
                am = (List<Amenities>)Session["AmenitiZaPretragu"];
            }
            else
            {
                am = new List<Amenities>();
            }
            foreach (var item in lista)
            {
                foreach (var item1 in item.Value)
                {
                    if (item1.Naziv == Naziv)
                    {
                        am.Add(item1);
                    }
                }
            }
            foreach (var item2 in ap)
            {
                foreach (var item3 in item2.Sadrzaj)
                {
                    bool p = false;
                    foreach (var item in am)
                    {
                        if (item.Naziv == item3.Naziv)
                        {
                            p = true;
                        }

                    }
                    if (p == true)
                    {
                        ap1.Add(item2);
                    }
                }
            }
            if (Naziv == "Apartment")
            {
                ap1 = new List<Apartman>();
                foreach (var item in ap)
                {
                    if (item.tip == Tip.Apartman)
                    {
                        ap1.Add(item);
                    }
                }
            }
            else if (Naziv == "Room")
            {
                ap1 = new List<Apartman>();
                foreach (var item in ap)
                {
                    if (item.tip == Tip.Soba)
                    {
                        ap1.Add(item);
                    }
                }
            }
            if (Naziv == "Active")
            {
                ap1 = new List<Apartman>();
                foreach (var item in ap)
                {
                    if (item.status == Status.Aktivan)
                    {
                        ap1.Add(item);
                    }
                }
            }
            else if (Naziv == "Unactive")
            {
                ap1 = new List<Apartman>();
                foreach (var item in ap)
                {
                    if (item.status == Status.Neaktivan)
                    {
                        ap1.Add(item);
                    }
                }
            }
            ViewBag.sadrzaj = lista;
            ViewBag.Apartmani = ap1;
            return View("Domacin");
        }

        public ActionResult FilterListaApartmana(string Naziv)
        {
            Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];

            List<Apartman> ap = (List<Apartman>)HttpContext.Cache["Apartmani"];
            List<Apartman> ap1 = new List<Apartman>();
            List<Amenities> am = new List<Amenities>();
            if ((List<Amenities>)Session["AmenitiZaPretragu"] != null)
            {
                am = (List<Amenities>)Session["AmenitiZaPretragu"];
            }
            else
            {
                am = new List<Amenities>();
            }
            foreach (var item in lista)
            {
                foreach (var item1 in item.Value)
                {
                    if (item1.Naziv == Naziv)
                    {
                        am.Add(item1);
                    }
                }
            }
            foreach (var item2 in ap)
            {
                foreach (var item3 in item2.Sadrzaj)
                {
                    bool p = false;
                    foreach (var item in am)
                    {
                        if (item.Naziv == item3.Naziv)
                        {
                            p = true;
                        }

                    }
                    if (p == true)
                    {
                        ap1.Add(item2);
                    }
                }
            }
            if (Naziv == "Apartment")
            {
                ap1 = new List<Apartman>();
                foreach (var item in ap)
                {
                    if (item.tip == Tip.Apartman)
                    {
                        ap1.Add(item);
                    }
                }
            }
            else if (Naziv == "Room")
            {
                ap1 = new List<Apartman>();
                foreach (var item in ap)
                {
                    if (item.tip == Tip.Soba)
                    {
                        ap1.Add(item);
                    }
                }
            }
            if (Naziv == "Active")
            {
                ap1 = new List<Apartman>();
                foreach (var item in ap)
                {
                    if (item.status == Status.Aktivan)
                    {
                        ap1.Add(item);
                    }
                }
            }
            else if (Naziv == "Unactive")
            {
                ap1 = new List<Apartman>();
                foreach (var item in ap)
                {
                    if (item.status == Status.Neaktivan)
                    {
                        ap1.Add(item);
                    }
                }
            }
            ViewBag.sadrzaj = lista;
            ViewBag.Apartmani = ap1;
            return View("ListaApartmana");
        }
        public ActionResult FilterGost(string Naziv)
        {
            Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];

            List<Apartman> ap = (List<Apartman>)HttpContext.Cache["Apartmani"];
            List<Apartman> ap1 = new List<Apartman>();
            List<Apartman> ap2 = new List<Apartman>();
            List<Amenities> am = new List<Amenities>();
            if ((List<Amenities>)Session["AmenitiZaPretragu"] != null)
            {
                am = (List<Amenities>)Session["AmenitiZaPretragu"];
            }
            else
            {
                am = new List<Amenities>();
            }
            foreach (var item in lista)
            {
                foreach (var item1 in item.Value)
                {
                    if (item1.Naziv == Naziv)
                    {
                        am.Add(item1);
                    }
                }
            }
            foreach (var item2 in ap)
            {
                foreach (var item3 in item2.Sadrzaj)
                {
                    bool p = false;
                    foreach (var item in am)
                    {
                        if (item.Naziv == item3.Naziv)
                        {
                            p = true;
                        }

                    }
                    if (p == true)
                    {
                        ap1.Add(item2);
                    }
                }
            }
            foreach (var item in ap1)
            {
                if (item.status == Status.Aktivan)
                {
                    ap2.Add(item);
                }
            }
            ap1 = ap2;
            if (Naziv == "Apartment")
            {
                ap1 = new List<Apartman>();
                foreach (var item in ap)
                {
                    if (item.status == Status.Aktivan && item.tip == Tip.Apartman)
                    {
                        ap1.Add(item);
                    }
                }
            }
            else if (Naziv == "Room")
            {
                ap1 = new List<Apartman>();
                foreach (var item in ap)
                {
                    if (item.status == Status.Aktivan && item.tip == Tip.Soba)
                    {
                        ap1.Add(item);
                    }
                }
            }
            ViewBag.sadrzaj = lista;
            ViewBag.Apartmani = ap1;
            return View("Gost");
        }
        public ActionResult FilterApartmaniNeulogovani(string Naziv)
        {
            Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];

            List<Apartman> ap = (List<Apartman>)HttpContext.Cache["Apartmani"];
            List<Apartman> ap1 = new List<Apartman>();
            List<Apartman> ap2 = new List<Apartman>();
            List<Amenities> am = new List<Amenities>();
            if ((List<Amenities>)Session["AmenitiZaPretragu"] != null)
            {
                am = (List<Amenities>)Session["AmenitiZaPretragu"];
            }
            else
            {
                am = new List<Amenities>();
            }
            foreach (var item in lista)
            {
                foreach (var item1 in item.Value)
                {
                    if (item1.Naziv == Naziv)
                    {
                        am.Add(item1);
                    }
                }
            }
            foreach (var item2 in ap)
            {
                foreach (var item3 in item2.Sadrzaj)
                {
                    bool p = false;
                    foreach (var item in am)
                    {
                        if (item.Naziv == item3.Naziv)
                        {
                            p = true;
                        }

                    }
                    if (p == true)
                    {
                        ap1.Add(item2);
                    }
                }
            }
            foreach (var item in ap1)
            {
                if (item.status == Status.Aktivan)
                {
                    ap2.Add(item);
                }
            }
            ap1 = ap2;
            if (Naziv == "Apartment")
            {
                ap1 = new List<Apartman>();
                foreach (var item in ap)
                {
                    if (item.status == Status.Aktivan && item.tip == Tip.Apartman)
                    {
                        ap1.Add(item);
                    }
                }
            }
            else if (Naziv == "Room")
            {
                ap1 = new List<Apartman>();
                foreach (var item in ap)
                {
                    if (item.status == Status.Aktivan && item.tip == Tip.Soba)
                    {
                        ap1.Add(item);
                    }
                }
            }
            ViewBag.sadrzaj = lista;
            ViewBag.Apartmani = ap1;
            return View("Apartmani");
        }
        [HttpPost]
        public ActionResult PretragaDugmeAdmin()
        {
            List<Apartman> lista = (List<Apartman>)HttpContext.Cache["Apartmani"];
            Korisnik k = (Korisnik)Session["Ulogovan"];
            List<Apartman> pretrazeno = null;
            if (Request["Mesto"] != null && Request["Mesto"] != "")
            {
                pretrazeno = new List<Apartman>();
                foreach (var item in lista)
                {
                    if (item.lokacija.adresa.Mesto == Request["Mesto"])
                        pretrazeno.Add(item);
                }
            }
            if (Request["OdCene"] != null && Request["OdCene"] != "")
            {
                if (pretrazeno == null)
                {
                    pretrazeno = new List<Apartman>();
                    foreach (var item in lista)
                    {
                        if (item.Broj_soba > int.Parse(Request["OdCene"]))
                            pretrazeno.Add(item);
                    }
                }
                else
                {
                    List<Apartman> temp = new List<Apartman>();

                    foreach (var item in pretrazeno)
                    {
                        if (item.Broj_soba > int.Parse(Request["OdCene"]))
                            temp.Add(item);
                    }
                    pretrazeno = temp;
                }
            }
            if (Request["DoCene"] != null && Request["DoCene"] != "")
            {
                if (pretrazeno == null)
                {
                    pretrazeno = new List<Apartman>();
                    foreach (var item in lista)
                    {
                        if (item.Broj_soba < int.Parse(Request["DoCene"]))
                            pretrazeno.Add(item);
                    }
                }
                else
                {
                    List<Apartman> temp = new List<Apartman>();
                    foreach (var item in pretrazeno)
                    {
                        if (item.Broj_soba < int.Parse(Request["DoCene"]))
                            temp.Add(item);
                    }
                    pretrazeno = temp;
                }
            }
            if (Request["OdSobe"] != null && Request["OdSobe"] != "")
            {
                if (pretrazeno == null)
                {
                    pretrazeno = new List<Apartman>();
                    foreach (var item in lista)
                    {
                        if (item.Broj_soba > int.Parse(Request["OdSobe"]))
                            pretrazeno.Add(item);
                    }
                }
                else
                {
                    List<Apartman> temp = new List<Apartman>();

                    foreach (var item in pretrazeno)
                    {
                        if (item.Broj_soba > int.Parse(Request["OdSobe"]))
                            temp.Add(item);
                    }
                    pretrazeno = temp;
                }
            }
            if (Request["DoSoba"] != null && Request["DoSoba"] != "")
            {
                if (pretrazeno == null)
                {
                    pretrazeno = new List<Apartman>();
                    foreach (var item in lista)
                    {
                        if (item.Broj_soba < int.Parse(Request["DoSoba"]))
                            pretrazeno.Add(item);
                    }
                }
                else
                {
                    List<Apartman> temp = new List<Apartman>();
                    foreach (var item in pretrazeno)
                    {
                        if (item.Broj_soba < int.Parse(Request["DoSoba"]))
                            temp.Add(item);
                    }
                    pretrazeno = temp;
                }
            }

            if (Request["BrOsoba"] != null && Request["BrOsoba"] != "")
            {
                if (pretrazeno == null)
                {
                    pretrazeno = new List<Apartman>();
                    foreach (var item in lista)
                    {
                        if (item.Broj_gostiju == int.Parse(Request["BrOsoba"]))
                            pretrazeno.Add(item);
                    }
                }
                else
                {
                    List<Apartman> temp = new List<Apartman>();
                    foreach (var item in pretrazeno)
                    {
                        if (item.Broj_gostiju == int.Parse(Request["BrOsoba"]))
                            temp.Add(item);
                    }
                    pretrazeno = temp;
                }
            }
            if (Request["sort"] != null)
            {
                if (pretrazeno == null)
                {
                    if (Request["sort"] == "yes")
                    {
                        for (int i = 0; i < lista.Count - 1; i++)
                        {
                            for (int j = i + 1; j < lista.Count; j++)
                            {
                                if (lista[i].Cena_po_noci > lista[j].Cena_po_noci)
                                {
                                    Apartman temp = lista[i];
                                    lista[i] = lista[j];
                                    lista[j] = temp;
                                }
                            }
                        }

                    }
                    else
                    {
                        for (int i = 0; i < lista.Count - 1; i++)
                        {
                            for (int j = i + 1; j < lista.Count; j++)
                            {
                                if (lista[i].Cena_po_noci < lista[j].Cena_po_noci)
                                {
                                    Apartman temp = lista[i];
                                    lista[i] = lista[j];
                                    lista[j] = temp;
                                }
                            }
                        }

                    }
                    pretrazeno = lista;
                }
                else
                {
                    if (Request["sort"] == "yes")
                    {
                        for (int i = 0; i < pretrazeno.Count - 1; i++)
                        {
                            for (int j = i + 1; j < pretrazeno.Count; j++)
                            {
                                if (pretrazeno[i].Cena_po_noci > pretrazeno[j].Cena_po_noci)
                                {
                                    Apartman temp = pretrazeno[i];
                                    pretrazeno[i] = pretrazeno[j];
                                    pretrazeno[j] = temp;
                                }
                            }
                        }

                    }
                    else
                    {
                        for (int i = 0; i < pretrazeno.Count - 1; i++)
                        {
                            for (int j = i + 1; j < pretrazeno.Count; j++)
                            {
                                if (pretrazeno[i].Cena_po_noci < pretrazeno[j].Cena_po_noci)
                                {
                                    Apartman temp = pretrazeno[i];
                                    pretrazeno[i] = pretrazeno[j];
                                    pretrazeno[j] = temp;
                                }
                            }
                        }

                    }
                }
            }
            Session["Pretragal"] = null;
            Dictionary<string, List<Amenities>> sad = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            if (pretrazeno == null)
            {
                pretrazeno = lista;
            }

            ViewBag.Apartmani = pretrazeno;
            ViewBag.sadrzaj = sad;
            return View("ListaApartmana");
        }
        [HttpPost]
        public ActionResult PretragaDugmeGost()
        {
            List<Apartman> listaf = (List<Apartman>)HttpContext.Cache["Apartmani"];

            Korisnik k = (Korisnik)Session["Ulogovan"];
            List<Apartman> lista = new List<Apartman>();
            foreach (var item in listaf)
            {
                if (item.status == Status.Aktivan)
                {
                    lista.Add(item);
                }
            }
            List<Apartman> pretrazeno = null;
            if (Request["Mesto"] != null && Request["Mesto"] != "")
            {
                pretrazeno = new List<Apartman>();
                foreach (var item in lista)
                {
                    if (item.lokacija.adresa.Mesto == Request["Mesto"])
                        pretrazeno.Add(item);
                }
            }
            if (Request["OdCene"] != null && Request["OdCene"] != "")
            {
                if (pretrazeno == null)
                {
                    pretrazeno = new List<Apartman>();
                    foreach (var item in lista)
                    {
                        if (item.Broj_soba > int.Parse(Request["OdCene"]))
                            pretrazeno.Add(item);
                    }
                }
                else
                {
                    List<Apartman> temp = new List<Apartman>();

                    foreach (var item in pretrazeno)
                    {
                        if (item.Broj_soba > int.Parse(Request["OdCene"]))
                            temp.Add(item);
                    }
                    pretrazeno = temp;
                }
            }
            if (Request["DoCene"] != null && Request["DoCene"] != "")
            {
                if (pretrazeno == null)
                {
                    pretrazeno = new List<Apartman>();
                    foreach (var item in lista)
                    {
                        if (item.Broj_soba < int.Parse(Request["DoCene"]))
                            pretrazeno.Add(item);
                    }
                }
                else
                {
                    List<Apartman> temp = new List<Apartman>();
                    foreach (var item in pretrazeno)
                    {
                        if (item.Broj_soba < int.Parse(Request["DoCene"]))
                            temp.Add(item);
                    }
                    pretrazeno = temp;
                }
            }
            if (Request["OdSobe"] != null && Request["OdSobe"] != "")
            {
                if (pretrazeno == null)
                {
                    pretrazeno = new List<Apartman>();
                    foreach (var item in lista)
                    {
                        if (item.Broj_soba > int.Parse(Request["OdSobe"]))
                            pretrazeno.Add(item);
                    }
                }
                else
                {
                    List<Apartman> temp = new List<Apartman>();

                    foreach (var item in pretrazeno)
                    {
                        if (item.Broj_soba > int.Parse(Request["OdSobe"]))
                            temp.Add(item);
                    }
                    pretrazeno = temp;
                }
            }
            if (Request["DoSoba"] != null && Request["DoSoba"] != "")
            {
                if (pretrazeno == null)
                {
                    pretrazeno = new List<Apartman>();
                    foreach (var item in lista)
                    {
                        if (item.Broj_soba < int.Parse(Request["DoSoba"]))
                            pretrazeno.Add(item);
                    }
                }
                else
                {
                    List<Apartman> temp = new List<Apartman>();
                    foreach (var item in pretrazeno)
                    {
                        if (item.Broj_soba < int.Parse(Request["DoSoba"]))
                            temp.Add(item);
                    }
                    pretrazeno = temp;
                }
            }

            if (Request["BrOsoba"] != null && Request["BrOsoba"] != "")
            {
                if (pretrazeno == null)
                {
                    pretrazeno = new List<Apartman>();
                    foreach (var item in lista)
                    {
                        if (item.Broj_gostiju == int.Parse(Request["BrOsoba"]))
                            pretrazeno.Add(item);
                    }
                }
                else
                {
                    List<Apartman> temp = new List<Apartman>();
                    foreach (var item in pretrazeno)
                    {
                        if (item.Broj_gostiju == int.Parse(Request["BrOsoba"]))
                            temp.Add(item);
                    }
                    pretrazeno = temp;
                }
            }
            if (Request["sort"] != null)
            {
                if (pretrazeno == null)
                {
                    if (Request["sort"] == "yes")
                    {
                        for (int i = 0; i < lista.Count - 1; i++)
                        {
                            for (int j = i + 1; j < lista.Count; j++)
                            {
                                if (lista[i].Cena_po_noci > lista[j].Cena_po_noci)
                                {
                                    Apartman temp = lista[i];
                                    lista[i] = lista[j];
                                    lista[j] = temp;
                                }
                            }
                        }

                    }
                    else
                    {
                        for (int i = 0; i < lista.Count - 1; i++)
                        {
                            for (int j = i + 1; j < lista.Count; j++)
                            {
                                if (lista[i].Cena_po_noci < lista[j].Cena_po_noci)
                                {
                                    Apartman temp = lista[i];
                                    lista[i] = lista[j];
                                    lista[j] = temp;
                                }
                            }
                        }

                    }
                    pretrazeno = lista;
                }
                else
                {
                    if (Request["sort"] == "yes")
                    {
                        for (int i = 0; i < pretrazeno.Count - 1; i++)
                        {
                            for (int j = i + 1; j < pretrazeno.Count; j++)
                            {
                                if (pretrazeno[i].Cena_po_noci > pretrazeno[j].Cena_po_noci)
                                {
                                    Apartman temp = pretrazeno[i];
                                    pretrazeno[i] = pretrazeno[j];
                                    pretrazeno[j] = temp;
                                }
                            }
                        }

                    }
                    else
                    {
                        for (int i = 0; i < pretrazeno.Count - 1; i++)
                        {
                            for (int j = i + 1; j < pretrazeno.Count; j++)
                            {
                                if (pretrazeno[i].Cena_po_noci < pretrazeno[j].Cena_po_noci)
                                {
                                    Apartman temp = pretrazeno[i];
                                    pretrazeno[i] = pretrazeno[j];
                                    pretrazeno[j] = temp;
                                }
                            }
                        }

                    }
                }
            }
            Session["Pretragal"] = null;
            Dictionary<string, List<Amenities>> sad = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            if (pretrazeno == null)
            {
                pretrazeno = lista;
            }
            ViewBag.Apartmani = pretrazeno;
            ViewBag.sadrzaj = sad;
            return View("Gost");
        }
        [HttpPost]
        public ActionResult PretragaDugmeApartmani()
        {
            List<Apartman> listaf = (List<Apartman>)HttpContext.Cache["Apartmani"];

            Korisnik k = (Korisnik)Session["Ulogovan"];
            List<Apartman> lista = new List<Apartman>();
            foreach (var item in listaf)
            {
                if (item.status == Status.Aktivan)
                {
                    lista.Add(item);
                }
            }
            List<Apartman> pretrazeno = null;
            if (Request["Mesto"] != null && Request["Mesto"] != "")
            {
                pretrazeno = new List<Apartman>();
                foreach (var item in lista)
                {
                    if (item.lokacija.adresa.Mesto == Request["Mesto"])
                        pretrazeno.Add(item);
                }
            }
            if (Request["OdCene"] != null && Request["OdCene"] != "")
            {
                if (pretrazeno == null)
                {
                    pretrazeno = new List<Apartman>();
                    foreach (var item in lista)
                    {
                        if (item.Broj_soba > int.Parse(Request["OdCene"]))
                            pretrazeno.Add(item);
                    }
                }
                else
                {
                    List<Apartman> temp = new List<Apartman>();

                    foreach (var item in pretrazeno)
                    {
                        if (item.Broj_soba > int.Parse(Request["OdCene"]))
                            temp.Add(item);
                    }
                    pretrazeno = temp;
                }
            }
            if (Request["DoCene"] != null && Request["DoCene"] != "")
            {
                if (pretrazeno == null)
                {
                    pretrazeno = new List<Apartman>();
                    foreach (var item in lista)
                    {
                        if (item.Broj_soba < int.Parse(Request["DoCene"]))
                            pretrazeno.Add(item);
                    }
                }
                else
                {
                    List<Apartman> temp = new List<Apartman>();
                    foreach (var item in pretrazeno)
                    {
                        if (item.Broj_soba < int.Parse(Request["DoCene"]))
                            temp.Add(item);
                    }
                    pretrazeno = temp;
                }
            }
            if (Request["OdSobe"] != null && Request["OdSobe"] != "")
            {
                if (pretrazeno == null)
                {
                    pretrazeno = new List<Apartman>();
                    foreach (var item in lista)
                    {
                        if (item.Broj_soba > int.Parse(Request["OdSobe"]))
                            pretrazeno.Add(item);
                    }
                }
                else
                {
                    List<Apartman> temp = new List<Apartman>();

                    foreach (var item in pretrazeno)
                    {
                        if (item.Broj_soba > int.Parse(Request["OdSobe"]))
                            temp.Add(item);
                    }
                    pretrazeno = temp;
                }
            }
            if (Request["DoSoba"] != null && Request["DoSoba"] != "")
            {
                if (pretrazeno == null)
                {
                    pretrazeno = new List<Apartman>();
                    foreach (var item in lista)
                    {
                        if (item.Broj_soba < int.Parse(Request["DoSoba"]))
                            pretrazeno.Add(item);
                    }
                }
                else
                {
                    List<Apartman> temp = new List<Apartman>();
                    foreach (var item in pretrazeno)
                    {
                        if (item.Broj_soba < int.Parse(Request["DoSoba"]))
                            temp.Add(item);
                    }
                    pretrazeno = temp;
                }
            }

            if (Request["BrOsoba"] != null && Request["BrOsoba"] != "")
            {
                if (pretrazeno == null)
                {
                    pretrazeno = new List<Apartman>();
                    foreach (var item in lista)
                    {
                        if (item.Broj_gostiju == int.Parse(Request["BrOsoba"]))
                            pretrazeno.Add(item);
                    }
                }
                else
                {
                    List<Apartman> temp = new List<Apartman>();
                    foreach (var item in pretrazeno)
                    {
                        if (item.Broj_gostiju == int.Parse(Request["BrOsoba"]))
                            temp.Add(item);
                    }
                    pretrazeno = temp;
                }
            }
            if (Request["sort"] != null)
            {
                if (pretrazeno == null)
                {
                    if (Request["sort"] == "yes")
                    {
                        for (int i = 0; i < lista.Count - 1; i++)
                        {
                            for (int j = i + 1; j < lista.Count; j++)
                            {
                                if (lista[i].Cena_po_noci > lista[j].Cena_po_noci)
                                {
                                    Apartman temp = lista[i];
                                    lista[i] = lista[j];
                                    lista[j] = temp;
                                }
                            }
                        }

                    }
                    else
                    {
                        for (int i = 0; i < lista.Count - 1; i++)
                        {
                            for (int j = i + 1; j < lista.Count; j++)
                            {
                                if (lista[i].Cena_po_noci < lista[j].Cena_po_noci)
                                {
                                    Apartman temp = lista[i];
                                    lista[i] = lista[j];
                                    lista[j] = temp;
                                }
                            }
                        }

                    }
                    pretrazeno = lista;
                }
                else
                {
                    if (Request["sort"] == "yes")
                    {
                        for (int i = 0; i < pretrazeno.Count - 1; i++)
                        {
                            for (int j = i + 1; j < pretrazeno.Count; j++)
                            {
                                if (pretrazeno[i].Cena_po_noci > pretrazeno[j].Cena_po_noci)
                                {
                                    Apartman temp = pretrazeno[i];
                                    pretrazeno[i] = pretrazeno[j];
                                    pretrazeno[j] = temp;
                                }
                            }
                        }

                    }
                    else
                    {
                        for (int i = 0; i < pretrazeno.Count - 1; i++)
                        {
                            for (int j = i + 1; j < pretrazeno.Count; j++)
                            {
                                if (pretrazeno[i].Cena_po_noci < pretrazeno[j].Cena_po_noci)
                                {
                                    Apartman temp = pretrazeno[i];
                                    pretrazeno[i] = pretrazeno[j];
                                    pretrazeno[j] = temp;
                                }
                            }
                        }

                    }
                }
            }
            Session["Pretragal"] = null;
            Dictionary<string, List<Amenities>> sad = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            if (pretrazeno == null)
            {
                pretrazeno = lista;
            }
            ViewBag.Apartmani = pretrazeno;
            ViewBag.sadrzaj = sad;
            return View("Apartmani");
        }
        public ActionResult PretrazivanjeUsera()
        {
            Dictionary<string, Korisnik> recnik = (Dictionary<string, Korisnik>)HttpContext.Cache["Korisnici"];
            Dictionary<string, Korisnik> povratno = null;
            if (Request["Uloga"] != null)
            {
                povratno = new Dictionary<string, Korisnik>();
                if (Request["Uloga"] == Uloga.Administrator.ToString())
                {
                    foreach (var item in recnik.Values)
                    {
                        if (item.Uloga == Uloga.Administrator)
                            povratno.Add(item.Korisnicko_ime, item);
                    }
                }
                else if (Request["Uloga"] == Uloga.Domacin.ToString())
                {
                    foreach (var item in recnik.Values)
                    {
                        if (item.Uloga == Uloga.Domacin)
                            povratno.Add(item.Korisnicko_ime, item);
                    }
                }
                else
                {
                    foreach (var item in recnik.Values)
                    {
                        if (item.Uloga == Uloga.Gost)
                            povratno.Add(item.Korisnicko_ime, item);
                    }
                }
            }
            if (Request["Polovi"] != null)
            {
                if (povratno == null)
                {
                    povratno = new Dictionary<string, Korisnik>();
                    if (Request["Polovi"] == Pol.Musko.ToString())
                    {
                        foreach (var item in recnik.Values)
                        {
                            if (item.Pol == Pol.Musko)
                                povratno.Add(item.Korisnicko_ime, item);
                        }
                    }
                    else
                    {
                        foreach (var item in recnik.Values)
                        {
                            if (item.Pol == Pol.Zensko)
                                povratno.Add(item.Korisnicko_ime, item);
                        }
                    }
                }
                else
                {
                    Dictionary<string, Korisnik> temp = new Dictionary<string, Korisnik>();
                    if (Request["Polovi"] == Pol.Musko.ToString())
                    {
                        foreach (var item in povratno.Values)
                        {
                            if (item.Pol == Pol.Musko)
                                temp.Add(item.Korisnicko_ime, item);
                        }
                    }
                    else
                    {
                        foreach (var item in povratno.Values)
                        {
                            if (item.Pol == Pol.Zensko)
                                temp.Add(item.Korisnicko_ime, item);
                        }
                    }
                    povratno = temp;
                }
            }
            if (Request["User"] != null && Request["User"] != "")
            {
                if (povratno != null)
                {
                    if (povratno.ContainsKey(Request["User"]))
                    {
                        Korisnik temp = povratno[Request["User"]];
                        povratno.Clear();
                        povratno.Add(temp.Korisnicko_ime, temp);
                    }
                    else
                    {
                        povratno = new Dictionary<string, Korisnik>();
                    }
                }
                else
                {
                    povratno = new Dictionary<string, Korisnik>();
                    if (recnik.ContainsKey(Request["User"]))
                    {
                        povratno.Add(Request["User"], recnik[Request["User"]]);
                    }

                }
            }
            if (povratno == null)
            {
                povratno = (Dictionary<string, Korisnik>)HttpContext.Cache["Korisnici"];
            }
            ViewBag.ListKorisnika = povratno.Values;
            Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            ViewBag.sadrzaj = lista;
            return View("Administrator");
        }
        [HttpPost]
        public ActionResult ApartmanDetalji(int Id)
        {
            List<Apartman> ap = (List<Apartman>)HttpContext.Cache["Apartmani"];
            Apartman ap1 = new Apartman();
            foreach (Apartman item in ap)
            {
                if (item.Identifikacija == Id)
                {
                    ap1 = item;
                }
            }
            Session["TrenutnoGleda"] = ap1;
            ViewBag.APARTMAN = ap1;
            List<Komentar> list5 = (List<Komentar>)HttpContext.Cache["Komentari"];
            List<Komentar> listic = new List<Komentar>();
            foreach (var item in list5)
            {
                if (item.apartman == ap1.Identifikacija)
                {
                    listic.Add(item);
                }
            }
            ViewBag.Komentari = listic;//
            return View("DetaljiApartmanaNeulogovan");
        }
        [HttpPost]
        public ActionResult GostDetalji(int Id)
        {
            List<Apartman> ap = (List<Apartman>)HttpContext.Cache["Apartmani"];
            Apartman ap1 = new Apartman();
            foreach (Apartman item in ap)
            {
                if (item.Identifikacija == Id)
                {
                    ap1 = item;
                }
            }
            Session["TrenutnoGleda"] = ap1;
            ViewBag.GostAPARTMAN = ap1;
            DateTime t = DateTime.Now;
            int god = t.Year;
            int mes = t.Month;
            int dani = DateTime.DaysInMonth(god, mes);
            DateTime poc = new DateTime(god, mes, 1);
            if (poc.DayOfWeek == DayOfWeek.Monday)
                ViewBag.pocetak = 0;
            else if (poc.DayOfWeek == DayOfWeek.Tuesday)
                ViewBag.pocetak = 1;
            else if (poc.DayOfWeek == DayOfWeek.Wednesday)
                ViewBag.pocetak = 2;
            else if (poc.DayOfWeek == DayOfWeek.Thursday)
                ViewBag.pocetak = 3;
            else if (poc.DayOfWeek == DayOfWeek.Friday)
                ViewBag.pocetak = 4;
            else if (poc.DayOfWeek == DayOfWeek.Saturday)
                ViewBag.pocetak = 5;
            else
                ViewBag.pocetak = 6;
            ViewBag.dana = dani;
            Korisnik k = (Korisnik)Session["Ulogovan"];
            if (k.Uloga == Uloga.Gost)
            {
                ViewBag.DozvolaZaBook = true;
            }
            List<Rezervacija> list = (List<Rezervacija>)HttpContext.Cache["Rezervacije"];
            List<Rezervacija> list1 = new List<Rezervacija>();
            foreach (Rezervacija item in list)
            {
                if (item.Gost == k.Korisnicko_ime)
                {
                    list1.Add(item);
                }
            }
            List<Rezervacija> list2 = new List<Rezervacija>();
            foreach (Rezervacija item in list1)
            {
                if (item.apartman == ap1.Identifikacija)
                {
                    list2.Add(item);
                }
            }
            bool uspesno = false;
            foreach (var item in list2)
            {
                if (item.Status == StatusRezervacije.Zavrsena || item.Status == StatusRezervacije.Odbijena)
                {
                    uspesno = true;

                }
            }
            if (uspesno)
            {
                ViewBag.Dozvola = true;
            }
            List<Komentar> list5 = (List<Komentar>)HttpContext.Cache["Komentari"];
            List<Komentar> listic = new List<Komentar>();
            foreach (var item in list5)
            {
                if (item.apartman == ap1.Identifikacija)
                {
                    listic.Add(item);
                }
            }
            ViewBag.Komentari = listic;//
            ViewBag.dozvoljeni = ap1.Dostupnost_po_datumima[0].Day - 1;
            Dictionary<string, int> potrebno = new Dictionary<string, int>();
            potrebno.Add("dani", ViewBag.dana);   //0
            potrebno.Add("prvidan", ViewBag.pocetak);  //1
            potrebno.Add("godina", god);   //2
            potrebno.Add("mesec", mes);  //3
            Session["trenutnipodacimeseca"] = potrebno;

            ViewBag.apartmanID = ap1.Identifikacija;
            ViewBag.mesec = poc.ToString("MMMM");
            return View("DetaljiApartmanaGost");
        }
        public ActionResult PrStatusaRezervacije()
        {
            int id = int.Parse(Request["id"]);
            List<Rezervacija> listar = (List<Rezervacija>)HttpContext.Cache["Rezervacije"];

            List<Apartman> listaA = (List<Apartman>)HttpContext.Cache["Apartmani"];
            int ident = -1;
            for (int i = 0; i < listar.Count; i++)
            {
                if (listar[i].ID == id)
                {
                    ident = listar[i].ID;
                    if (StatusRezervacije.Prihvacena.ToString() == Request["vrednost"])
                    {
                        listar[i].Status = StatusRezervacije.Prihvacena;
                    }
                    else if (Request["vrednost"] == StatusRezervacije.Odbijena.ToString())
                    {
                        listar[i].Status = StatusRezervacije.Odbijena;

                    }
                    else if (Request["vrednost"] == StatusRezervacije.Zavrsena.ToString())
                    {
                        listar[i].Status = StatusRezervacije.Zavrsena;
                    }
                }
            }
            b.SacuvajApartmane(listaA);
            b.SacuvajRezervacije(listar);
            List<int> opcije = new List<int>();
            foreach (var item in listar)
            {
                if (item.Status == StatusRezervacije.Kreirana)
                {
                    opcije.Add(1);
                }
                else if (item.Status == StatusRezervacije.Prihvacena)
                {
                    if (item.Pocetak.AddDays(item.Nocenje) < DateTime.Now)
                    {
                        opcije.Add(5);
                    }
                    else
                    {
                        opcije.Add(2);
                    }
                }
                else if (item.Status == StatusRezervacije.Odbijena)
                {
                    opcije.Add(3);

                }
                else if (item.Status == StatusRezervacije.Odustanak)
                {
                    opcije.Add(4);
                }
                else if (item.Status == StatusRezervacije.Zavrsena)
                {
                    opcije.Add(6);
                }

            }
            ViewBag.rezervacija = listar;
            ViewBag.opcija = opcije;
            return View("ListaRezervacije");
        }
        public ActionResult PrStatusaRezervacijeGost()
        {
            int id = int.Parse(Request["id"]);
            List<Rezervacija> listar = (List<Rezervacija>)HttpContext.Cache["Rezervacije"];

            List<Apartman> listaA = (List<Apartman>)HttpContext.Cache["Apartmani"];
            int ident = -1;
            for (int i = 0; i < listar.Count; i++)
            {
                if (listar[i].ID == id)
                {
                    ident = listar[i].ID;
                    if (StatusRezervacije.Prihvacena.ToString() == Request["vrednost"])
                    {
                        listar[i].Status = StatusRezervacije.Prihvacena;
                    }
                    else if (Request["vrednost"] == StatusRezervacije.Odbijena.ToString())
                    {
                        listar[i].Status = StatusRezervacije.Odbijena;
                    }
                    else if (Request["vrednost"] == StatusRezervacije.Zavrsena.ToString())
                    {
                        listar[i].Status = StatusRezervacije.Zavrsena;
                    }
                    else if (Request["vrednost"] == StatusRezervacije.Odustanak.ToString())
                    {
                        listar[i].Status = StatusRezervacije.Odustanak;
                    }
                }
            }
            b.SacuvajApartmane(listaA);
            b.SacuvajRezervacije(listar);
            List<int> opcije = new List<int>();
            foreach (var item in listar)
            {
                if (item.Status == StatusRezervacije.Kreirana)
                {
                    opcije.Add(1);
                }
                else if (item.Status == StatusRezervacije.Prihvacena)
                {
                    if (item.Pocetak.AddDays(item.Nocenje) < DateTime.Now)
                    {
                        opcije.Add(5);
                    }
                    else
                    {
                        opcije.Add(2);
                    }
                }
                else if (item.Status == StatusRezervacije.Odbijena)
                {
                    opcije.Add(3);

                }
                else if (item.Status == StatusRezervacije.Odustanak)
                {
                    opcije.Add(4);
                }
                else if (item.Status == StatusRezervacije.Zavrsena)
                {
                    opcije.Add(5);
                }
            }
            ViewBag.rezervacija = listar;
            ViewBag.opcija = opcije;
            return View("ListaRezervacijeGost");
        }
        public ActionResult VratiUGosta()
        {
            Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            ViewBag.sadrzaj = lista;
            ViewBag.Apartmani = (List<Apartman>)HttpContext.Cache["Apartmani"];
            return View("../Index/Gost");
        }
        [HttpPost]
        public ActionResult NapraviKomentar()
        {
            List<Komentar> lista = (List<Komentar>)HttpContext.Cache["Komentari"];
            List<Apartman> listaA = (List<Apartman>)HttpContext.Cache["Apartmani"];
            Dictionary<string, Korisnik> recnik = (Dictionary<string, Korisnik>)HttpContext.Cache["Korisnici"];
            Komentar k = new Komentar();
            k.Gost_komentar = ((Korisnik)Session["Ulogovan"]).Korisnicko_ime;
            if (lista.Count == 0)
                k.Id = 0;
            else
                k.Id = lista[lista.Count - 1].Id + 1;
            k.Tekst = Request["Tekst"];
            k.Ocena = int.Parse(Request["Ocena"]);
            int index = -1;
            for (int i = 0; i < listaA.Count; i++)
            {
                if (listaA[i].Identifikacija == int.Parse(Request["Id"]))
                {
                    k.apartman = listaA[i].Identifikacija;
                    if (listaA[i].komentari == null)
                        listaA[i].komentari = new List<int>();
                    index = i;
                }
            }
            k.Prikaz = false;
            lista.Add(k);
            b.SacuvajKomentare(lista);
            HttpContext.Cache["Komentari"] = lista;
            listaA[index].komentari.Add(k.Id);
            HttpContext.Cache["Apartmani"] = listaA;
            Dictionary<string, List<Amenities>> lista3 = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            ViewBag.sadrzaj = lista3;
            ViewBag.Apartmani = (List<Apartman>)HttpContext.Cache["Apartmani"];
            return View("Gost");
        }
        public ActionResult PrikaziSveKomentareAdmin()
        {
            List<Komentar> list = (List<Komentar>)HttpContext.Cache["Komentari"];
            ViewBag.Komentar = list;
            return View("KomentarAdmin");
        }
        [HttpPost]
        public ActionResult KomentarPrikaz(int Id)
        {
            List<Komentar> list = (List<Komentar>)HttpContext.Cache["Komentari"];
            for (int i = 0; i < list.Count; i++)
            {
                if (Id == list[i].Id)
                {
                    list[i].Prikaz = true;
                }
            }
            HttpContext.Cache["Komentari"] = list;
            b.SacuvajKomentare(list);
            return RedirectToAction("PrikaziSveKomentareAdmin");
        }
        [HttpPost]
        public ActionResult PretragaRezervacijeAdmin(string Tekst)
        {
            List<Rezervacija> listar = (List<Rezervacija>)HttpContext.Cache["Rezervacije"];
            List<Rezervacija> rezervacija = new List<Rezervacija>();
            foreach (Rezervacija item in listar)
            {
                if(item.Gost==Tekst)
                {
                    rezervacija.Add(item);
                }
            }

            List<int> opcije = new List<int>();
            foreach (var item in rezervacija)
            {
                if (item.Status == StatusRezervacije.Kreirana)
                {
                    opcije.Add(1);
                }
                else if (item.Status == StatusRezervacije.Prihvacena)
                {
                    if (item.Pocetak.AddDays(item.Nocenje) < DateTime.Now)
                    {
                        opcije.Add(5);
                    }
                    else
                    {
                        opcije.Add(2);
                    }
                }
                else if (item.Status == StatusRezervacije.Odbijena)
                {
                    opcije.Add(3);

                }
                else if (item.Status == StatusRezervacije.Odustanak)
                {
                    opcije.Add(4);
                }
                else if (item.Status == StatusRezervacije.Zavrsena)
                {
                    opcije.Add(6);
                }

            }
            
            ViewBag.rezervacija = rezervacija;
            ViewBag.opcija = opcije;
            return View("ListaRezervacije");
        }
        public ActionResult VratiUPocetnaIzKomentara()
        {
            Dictionary<string, List<Amenities>> lista = (Dictionary<string, List<Amenities>>)HttpContext.Cache["Amenities"];
            ViewBag.sadrzaj = lista;
            ViewBag.Apartmani = (List<Apartman>)HttpContext.Cache["Apartmani"];
            List<Komentar> list = (List<Komentar>)HttpContext.Cache["Komentari"];
            ViewBag.Komentar = list;
            return View("Apartmani");
        }
    }
}