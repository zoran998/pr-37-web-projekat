﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public enum Uloga { Administrator, Domacin, Gost };
public enum Pol { Musko, Zensko };
public class Korisnik
{
    public string Korisnicko_ime { get; set; }
    public string Lozinka { get; set; }
    public string Ime { get; set; }
    public string Prezime { get; set; }
    public Pol Pol { get; set; }
    public Uloga Uloga { get; set; }
    public List<int> Apartmani;//domacin
    public List<int> Iznajmljeni;//gost
    public List<int> Rezervacije;

    public Korisnik()
    {

    }

}

