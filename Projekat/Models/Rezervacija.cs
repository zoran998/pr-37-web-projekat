﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public enum StatusRezervacije { Kreirana, Odbijena, Odustanak, Prihvacena, Zavrsena }
public class Rezervacija
{
    public Rezervacija(DateTime pocetak, int nocenje, float ukupnaCena, StatusRezervacije status)
    {
        Pocetak = pocetak;
        Nocenje = nocenje;
        UkupnaCena = ukupnaCena;
        Status = status;
    }
    public Rezervacija()
    {

    }
    public int apartman { get; set; }
    public DateTime Pocetak { get; set; }
    public int Nocenje { get; set; }
    public float UkupnaCena { get; set; }
    public string Gost { get; set; }
    public StatusRezervacije Status { get; set; }
    public int ID { get; set; }

}
