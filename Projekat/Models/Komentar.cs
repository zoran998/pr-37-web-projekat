﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class Komentar
{
    public string Gost_komentar { get; set; }
    public int apartman { get; set; }
    public string Tekst { get; set; }
    public int Ocena { get; set; }
    public int Id { get; set; }
    public bool Prikaz { get; set; }
    public Komentar()
    {

    }
}
