﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;

public class Baza
{
    public void SaveXML(Dictionary<string, Korisnik> recnik)
    {
        XmlWriter xmlWriter = XmlWriter.Create(HttpContext.Current.Server.MapPath("~") + "/Files/korisnici.xml");

        xmlWriter.WriteStartDocument();
        xmlWriter.WriteStartElement("users");
        foreach (var item in recnik.Values)
        {
            if (item.Uloga != Uloga.Administrator)
            {
                xmlWriter.WriteStartElement("user");
                xmlWriter.WriteStartElement("username");
                xmlWriter.WriteString(item.Korisnicko_ime);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("password");
                xmlWriter.WriteString(item.Lozinka);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("name");
                xmlWriter.WriteString(item.Ime);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("surname");
                xmlWriter.WriteString(item.Prezime);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Sex");
                xmlWriter.WriteString(item.Pol.ToString());
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("role");
                xmlWriter.WriteString(item.Uloga.ToString());
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndElement();
            }
        }
        xmlWriter.WriteEndDocument();
        xmlWriter.Close();
    }
    public Dictionary<string, List<Amenities>> UcitajAmenities(Dictionary<string, List<Amenities>> recnik)
    {
        string xmldata = HttpContext.Current.Server.MapPath("~/Files/sadrzaj.xml");
        XmlDocument doc = new XmlDocument();
        try
        {
            doc.Load(xmldata);
            foreach (XmlNode node in doc.LastChild.ChildNodes)
            {
                string kljuc = node.Name.Replace("_", " ");
                if (!recnik.ContainsKey(kljuc))
                {
                    List<Amenities> lista = new List<Amenities>();
                    foreach (XmlNode item in node.ChildNodes)
                    {
                        XmlNode node1 = item.FirstChild;
                        Amenities a = new Amenities();
                        a.Naziv = node1.InnerText;
                        if (node1.NextSibling != null)
                        {
                            XmlNode node3 = node1.NextSibling;
                            a.Opis = node3.InnerText;
                        }
                        a.Id = lista.Count;
                        lista.Add(a);
                    }
                    recnik.Add(kljuc, lista);
                }
            }
        }
        catch (Exception)
        {
        }
        return recnik;
    }
    public void SacuvajRezervacije(List<Rezervacija> lista)
    {
        XmlWriter xmlWriter = XmlWriter.Create(HttpContext.Current.Server.MapPath("~") + "/Files/rezervacije.xml");
        xmlWriter.WriteStartDocument();
        xmlWriter.WriteStartElement("Rezervacije");
        foreach (var item in lista)
        {
            xmlWriter.WriteStartElement("Rezervacija");
            xmlWriter.WriteStartElement("ID");
            xmlWriter.WriteString(item.ID.ToString());
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("apartman");
            xmlWriter.WriteString(item.apartman.ToString());
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("gost");
            xmlWriter.WriteString(item.Gost);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("Nocenja");
            xmlWriter.WriteString(item.Nocenje.ToString());
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("Cena");
            xmlWriter.WriteString(item.UkupnaCena.ToString());
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("pocetak");
            xmlWriter.WriteString(item.Pocetak.ToString());
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("Status");
            xmlWriter.WriteString(item.Status.ToString());
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndElement();
        }
        xmlWriter.WriteEndElement();
        xmlWriter.WriteEndDocument();
        xmlWriter.Close();
    }
    public List<Rezervacija> UcitajRezervacije()
    {
        List<Rezervacija> lista = new List<Rezervacija>();
        Dictionary<string, Korisnik> korisnici = (Dictionary<string, Korisnik>)HttpContext.Current.Cache["Korisnici"];
        string xmldata = HttpContext.Current.Server.MapPath("~/Files/rezervacije.xml");
        XmlDocument doc = new XmlDocument();
        try
        {
            doc.Load(xmldata);
            foreach (XmlNode node in doc.DocumentElement.ChildNodes)
            {
                XmlNode node1 = node.FirstChild;
                Rezervacija rez = new Rezervacija();
                rez.ID = int.Parse(node1.InnerText);
                node1 = node1.NextSibling;
                rez.apartman = int.Parse(node1.InnerText);
                node1 = node1.NextSibling;
                rez.Gost = node1.InnerText;
                node1 = node1.NextSibling;
                rez.Nocenje = int.Parse(node1.InnerText);
                node1 = node1.NextSibling;
                rez.UkupnaCena = float.Parse(node1.InnerText);
                node1 = node1.NextSibling;
                rez.Pocetak = DateTime.Parse(node1.InnerText);
                node1 = node1.NextSibling;
                if (StatusRezervacije.Kreirana.ToString() == node1.InnerText)
                {
                    rez.Status = StatusRezervacije.Kreirana;
                }
                else if (StatusRezervacije.Odbijena.ToString() == node1.InnerText)
                {
                    rez.Status = StatusRezervacije.Odbijena;
                }
                else if (StatusRezervacije.Odustanak.ToString() == node1.InnerText)
                {
                    rez.Status = StatusRezervacije.Odustanak;
                }
                else if (StatusRezervacije.Prihvacena.ToString() == node1.InnerText)
                {
                    rez.Status = StatusRezervacije.Prihvacena;
                }
                else
                {
                    rez.Status = StatusRezervacije.Zavrsena;
                }
                if (korisnici[rez.Gost].Rezervacije == null)
                    korisnici[rez.Gost].Rezervacije = new List<int>();
                korisnici[rez.Gost].Rezervacije.Add(rez.ID);
                HttpContext.Current.Cache["Korisnici"] = korisnici;
                lista.Add(rez);
            }

        }
        catch (Exception)
        {
        }
        return lista;
    }
    public void ReadXML()
    {
        string sadrzaj = HttpContext.Current.Server.MapPath("~/Files/korisnici.xml");
        XmlDocument document = new XmlDocument();
        document.Load(sadrzaj);

        List<Korisnik> lista = AddAdmin();
        foreach (XmlNode item in document.DocumentElement)
        {
            Korisnik korisnik = new Korisnik();
            XmlNode node;
            node = item.FirstChild;
            korisnik.Korisnicko_ime = node.InnerText;
            node = node.NextSibling;
            korisnik.Lozinka = node.InnerText;
            node = node.NextSibling;
            korisnik.Ime = node.InnerText;
            node = node.NextSibling;
            korisnik.Prezime = node.InnerText;
            node = node.NextSibling;
            string pol = node.InnerText;
            node = node.NextSibling;
            string uloga = node.InnerText;
            if (pol == "Musko")
            {
                korisnik.Pol = Pol.Musko;
            }
            else
            {
                korisnik.Pol = Pol.Zensko;
            }
            if (uloga == "Administrator")
            {
                korisnik.Uloga = Uloga.Administrator;
            }
            else if (uloga == "Domacin")
            {
                korisnik.Uloga = Uloga.Domacin;
            }
            else
            {
                korisnik.Uloga = Uloga.Gost;
            }
            lista.Add(korisnik);
        }
        Dictionary<string, Korisnik> korisnici = new Dictionary<string, Korisnik>();
        foreach (var item in lista)
        {
            korisnici.Add(item.Korisnicko_ime, item);
        }
        HttpContext.Current.Cache["Korisnici"] = korisnici;
    }
    public void SacuvajAmenities(Dictionary<string, List<Amenities>> recnik)
    {
        XmlWriter xmlWriter = XmlWriter.Create(HttpContext.Current.Server.MapPath("~") + "/Files/sadrzaj.xml");
        xmlWriter.WriteStartDocument();
        xmlWriter.WriteStartElement("Amenities");
        foreach (var item in recnik)
        {
            xmlWriter.WriteStartElement(item.Key.Replace(" ", "_").Replace("'", "___").Replace(",", "__").Replace("/", "____"));
            for (int i = 0; i < item.Value.Count; i++)
            {
                xmlWriter.WriteStartElement("Ameniti");
                xmlWriter.WriteStartElement("naziv");
                xmlWriter.WriteString(item.Value[i].Naziv);
                xmlWriter.WriteEndElement();

                if (item.Value[i].Opis != null)
                {
                    xmlWriter.WriteStartElement("opis");
                    xmlWriter.WriteString(item.Value[i].Opis);
                    xmlWriter.WriteEndElement();
                }
                xmlWriter.WriteEndElement();
            }
            xmlWriter.WriteEndElement();
        }
        xmlWriter.WriteEndElement();
        xmlWriter.WriteEndDocument();
        xmlWriter.Close();
    }

    public List<Apartman> UcitajApartmane()
    {
      
        Dictionary<string, Korisnik> recnik = (Dictionary<string, Korisnik>)HttpContext.Current.Cache["Korisnici"];

        List<Apartman> lista = new List<Apartman>();
        string xmldata = HttpContext.Current.Server.MapPath("~/Files/Apartmani.xml");
        XmlDocument doc = new XmlDocument();
        try
        {
            doc.Load(xmldata);
            foreach (XmlNode node in doc.DocumentElement.ChildNodes)
            {
                Apartman a = new Apartman();
                XmlNode node1 = node.FirstChild;
                if (node1.InnerText == "Apartman")
                {
                    a.tip = Tip.Apartman;
                }
                else
                {
                    a.tip = Tip.Soba;
                }
                node1 = node1.NextSibling;
                a.Broj_soba = int.Parse(node1.InnerText);
                node1 = node1.NextSibling;
                a.Broj_gostiju = int.Parse(node1.InnerText);
                node1 = node1.NextSibling;
                a.lokacija = new Lokacija();
                a.lokacija.Sirina = float.Parse(node1.InnerText);
                node1 = node1.NextSibling;
                a.lokacija.Duzina = float.Parse(node1.InnerText);
                node1 = node1.NextSibling;
                a.lokacija.adresa = new Adresa();
                a.lokacija.adresa.Mesto = node1.InnerText;
                node1 = node1.NextSibling;
                a.lokacija.adresa.Posta = int.Parse(node1.InnerText);
                node1 = node1.NextSibling;
                a.lokacija.adresa.Ulica = node1.InnerText;
                node1 = node1.NextSibling;
                a.Cena_po_noci = float.Parse(node1.InnerText);
                node1 = node1.NextSibling;
                if (node1.InnerText == Status.Aktivan.ToString())
                {
                    a.status = Status.Aktivan;
                }
                else
                {
                    a.status = Status.Neaktivan;
                }
                node1 = node1.NextSibling;
                a.Slike = new List<string>();
                foreach (XmlNode item in node1)
                {
                    a.Slike.Add(item.InnerText);
                }
                node1 = node1.NextSibling;
                a.Identifikacija = int.Parse(node1.InnerText);
                node1 = node1.NextSibling;
                a.Domacin = recnik[node1.InnerText].Korisnicko_ime;
                node1 = node1.NextSibling;
                a.Sadrzaj = new List<Amenities>();
                foreach (XmlNode item in node1)
                {
                    XmlNode za = item.FirstChild;
                    Amenities am = new Amenities();
                    am.Id = int.Parse(za.InnerText);
                    za = za.NextSibling;
                    am.Naziv = za.InnerText;
                    if (za.NextSibling != null)
                    {
                        za = za.NextSibling;
                        am.Opis = za.InnerText;
                    }
                    a.Sadrzaj.Add(am);
                }
                node1 = node1.NextSibling;
                if (recnik[a.Domacin].Iznajmljeni == null)
                {
                    recnik[a.Domacin].Iznajmljeni = new List<int>();
                }
                recnik[a.Domacin].Iznajmljeni.Add(a.Identifikacija);
                a.Datumi_za_izdavanje = new List<DateTime>();
                foreach (XmlNode node2 in node1)
                {
                    DateTime novi = DateTime.Parse(node2.InnerText);
                    a.Datumi_za_izdavanje.Add(novi);
                }
                node1 = node1.NextSibling;
                a.Vreme_za_prijavu = TimeSpan.Parse(node1.InnerText);
                node1 = node1.NextSibling;
                a.Vreme_za_odjavu = TimeSpan.Parse(node1.InnerText);
                node1 = node1.NextSibling;
                if (node1.InnerText == "False")
                {
                    a.Obrisan = false;
                }
                else
                {
                    a.Obrisan = true;
                }
                node1 = node1.NextSibling;
                a.Dostupnost_po_datumima = new List<DateTime>();
                foreach (XmlNode node2 in node1)
                {
                    DateTime novi = DateTime.Parse(node2.InnerText);
                    a.Dostupnost_po_datumima.Add(novi);
                }
                node1 = node1.NextSibling;
                if(recnik[a.Domacin].Apartmani==null)
                {
                    recnik[a.Domacin].Apartmani = new List<int>();
                }
                recnik[a.Domacin].Apartmani.Add(a.Identifikacija);
                a.Rezervacija = new List<int>();
                lista.Add(a);

            }
        }
        catch (Exception)
        {
        }
        HttpContext.Current.Cache["Korisnici"] = recnik;
        return lista;
    }
    public void SacuvajApartmane(List<Apartman> l)
    {
        XmlWriter xmlWriter = XmlWriter.Create(HttpContext.Current.Server.MapPath("~") + "/Files/Apartmani.xml");
        xmlWriter.WriteStartDocument();
        xmlWriter.WriteStartElement("Apartmani");
        for (int i = 0; i < l.Count; i++)
        {
            SacuvajApartmanXML(l[i], xmlWriter);
        }
        xmlWriter.WriteEndElement();
        xmlWriter.WriteEndDocument();
        xmlWriter.Close();
    }
    public void SacuvajApartmanXML(Apartman a, XmlWriter xmlWriter)
    {
        xmlWriter.WriteStartElement("Apartman");
        xmlWriter.WriteStartElement("TipApartmana");
        xmlWriter.WriteString(a.tip.ToString());
        xmlWriter.WriteEndElement();
        xmlWriter.WriteStartElement("brojsoba");
        xmlWriter.WriteString(a.Broj_soba.ToString());
        xmlWriter.WriteEndElement();
        xmlWriter.WriteStartElement("broj");
        xmlWriter.WriteString(a.Broj_gostiju.ToString());
        xmlWriter.WriteEndElement();
        xmlWriter.WriteStartElement("sirina");
        xmlWriter.WriteString(a.lokacija.Sirina.ToString());
        xmlWriter.WriteEndElement();
        xmlWriter.WriteStartElement("duzina");
        xmlWriter.WriteString(a.lokacija.Duzina.ToString());
        xmlWriter.WriteEndElement();
        xmlWriter.WriteStartElement("mesto");
        xmlWriter.WriteString(a.lokacija.adresa.Mesto.ToString());
        xmlWriter.WriteEndElement();
        xmlWriter.WriteStartElement("postanskibroj");
        xmlWriter.WriteString(a.lokacija.adresa.Posta.ToString());
        xmlWriter.WriteEndElement();
        xmlWriter.WriteStartElement("ulica");
        xmlWriter.WriteString(a.lokacija.adresa.Ulica.ToString());
        xmlWriter.WriteEndElement();
        xmlWriter.WriteStartElement("cena");
        xmlWriter.WriteString(a.Cena_po_noci.ToString());
        xmlWriter.WriteEndElement();
        xmlWriter.WriteStartElement("status");
        xmlWriter.WriteString(a.status.ToString());
        xmlWriter.WriteEndElement();
        xmlWriter.WriteStartElement("slike");
        if (a.Slike != null)
        {
            for (int i = 0; i < a.Slike.Count; i++)
            {
                xmlWriter.WriteStartElement("slika");
                xmlWriter.WriteString(a.Slike[i]);
                xmlWriter.WriteEndElement();
            }
        }
        xmlWriter.WriteEndElement();
        xmlWriter.WriteStartElement("indetifikacija");
        xmlWriter.WriteString(a.Identifikacija.ToString());
        xmlWriter.WriteEndElement();
        xmlWriter.WriteStartElement("korisnik");
        xmlWriter.WriteString(a.Domacin);
        xmlWriter.WriteEndElement();
        xmlWriter.WriteStartElement("listaSadrzaja");
        if (a.Sadrzaj != null)
        {
            for (int i = 0; i < a.Sadrzaj.Count; i++)
            {
                xmlWriter.WriteStartElement("sadrzaj");
                xmlWriter.WriteStartElement("id");
                xmlWriter.WriteString(a.Sadrzaj[i].Id.ToString());
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("naziv");
                xmlWriter.WriteString(a.Sadrzaj[i].Naziv);
                xmlWriter.WriteEndElement();
                if (a.Sadrzaj[i].Opis != null)
                {
                    xmlWriter.WriteStartElement("opis");
                    xmlWriter.WriteString(a.Sadrzaj[i].Opis);
                    xmlWriter.WriteEndElement();
                }
                xmlWriter.WriteEndElement();
            }
        }
        xmlWriter.WriteEndElement();
        xmlWriter.WriteStartElement("listaDatumaIzdavanja");
        if (a.Datumi_za_izdavanje != null)
        {
            for (int i = 0; i < a.Datumi_za_izdavanje.Count; i++)
            {
                xmlWriter.WriteStartElement("Dan");
                xmlWriter.WriteString(a.Datumi_za_izdavanje[i].ToString());
                xmlWriter.WriteEndElement();
            }

        }
        xmlWriter.WriteEndElement();
        xmlWriter.WriteStartElement("vremeprijave");
        xmlWriter.WriteString(a.Vreme_za_prijavu.ToString());
        xmlWriter.WriteEndElement();
        xmlWriter.WriteStartElement("vremeodjave");
        xmlWriter.WriteString(a.Vreme_za_odjavu.ToString());
        xmlWriter.WriteEndElement();
        xmlWriter.WriteStartElement("Obrisan");
        xmlWriter.WriteString(a.Obrisan.ToString());
        xmlWriter.WriteEndElement();
        xmlWriter.WriteStartElement("ListaDostupnosti");
        if (a.Dostupnost_po_datumima != null)
        {
            for (int i = 0; i < a.Dostupnost_po_datumima.Count; i++)
            {
                xmlWriter.WriteStartElement("Dan");
                xmlWriter.WriteString(a.Dostupnost_po_datumima[i].ToString());
                xmlWriter.WriteEndElement();
            }
        }
        xmlWriter.WriteEndElement();
        xmlWriter.WriteEndElement();
    }
    public List<Korisnik> AddAdmin()
    {
        List<Korisnik> admini = new List<Korisnik>();
        using (StreamReader fajl = new StreamReader(HttpContext.Current.Server.MapPath("~") + @"\Files\ListaAdmina.txt"))
        {
            string linija;
            while ((linija = fajl.ReadLine()) != null)
            {
                Korisnik korisnik = new Korisnik();
                string[] vrednosti = linija.Split('|');
                korisnik.Korisnicko_ime = vrednosti[0];
                korisnik.Lozinka = vrednosti[1];
                korisnik.Ime = vrednosti[2];
                korisnik.Prezime = vrednosti[3];
                if (vrednosti[4] == "Musko")
                {
                    korisnik.Pol = Pol.Musko;
                }
                else
                {
                    korisnik.Pol = Pol.Zensko;
                }
                korisnik.Uloga = Uloga.Administrator;

                admini.Add(korisnik);
            }
            fajl.Close();
        }

        return admini;
    }
    public List<Komentar> UcitajKomentare()
    {
        List<Komentar> lista = new List<Komentar>();
        List<Apartman> listaA = (List<Apartman>)HttpContext.Current.Cache["Apartmani"];
        string xmldata = HttpContext.Current.Server.MapPath("~/Files/komentari.xml");
        XmlDocument doc = new XmlDocument();
        try
        {
            doc.Load(xmldata);
            foreach (XmlNode node in doc.DocumentElement.ChildNodes)
            {
                Komentar k = new Komentar();
                XmlNode node1 = node.FirstChild;
                k.Id = int.Parse(node1.InnerText);
                node1 = node1.NextSibling;
                k.Tekst = node1.InnerText;
                node1 = node1.NextSibling;
                k.Ocena = int.Parse(node1.InnerText);
                node1 = node1.NextSibling;
                k.apartman = int.Parse(node1.InnerText);
                node1 = node1.NextSibling;
                k.Gost_komentar = node1.InnerText;
                node1 = node1.NextSibling;
                if (node1.InnerText == "False")
                {
                    k.Prikaz = false;
                }
                else
                {
                    k.Prikaz = true;
                }
                foreach (var item in listaA)
                {
                    if (item.Identifikacija == k.apartman)
                    {

                        if (item.komentari == null)
                        {
                            item.komentari = new List<int>();
                        }
                        item.komentari.Add(k.Id);
                    }
                }
                lista.Add(k);
                HttpContext.Current.Cache["Apartmani"] = listaA;
            }
        }
        catch (Exception)
        {
        }
        return lista;
    }
    public void SacuvajKomentare(List<Komentar> lista)
    {
        XmlWriter xmlWriter = XmlWriter.Create(HttpContext.Current.Server.MapPath("~") + "/Files/komentari.xml");
        xmlWriter.WriteStartDocument();
        xmlWriter.WriteStartElement("Komentari");
        foreach (var item in lista)
        {
            xmlWriter.WriteStartElement("Komentar");
            xmlWriter.WriteStartElement("IDK");
            xmlWriter.WriteString(item.Id.ToString());
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("tekst");
            xmlWriter.WriteString(item.Tekst);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("Ocena");
            xmlWriter.WriteString(item.Ocena.ToString());
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("Apartman");
            xmlWriter.WriteString(item.apartman.ToString());
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("Gost");
            xmlWriter.WriteString(item.Gost_komentar);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("Prikaz");
            xmlWriter.WriteString(item.Prikaz.ToString());
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndElement();
        }
        xmlWriter.WriteEndElement();
        xmlWriter.WriteEndDocument();
        xmlWriter.Close();
    }
    public Dictionary<string, Korisnik> UcitajKorisnike(Dictionary<string, Korisnik> recnik)
    {
        string xmldata = HttpContext.Current.Server.MapPath("~/Files/Korisnici.xml");
        XmlDocument doc = new XmlDocument();
        try
        {
            doc.Load(xmldata);
            foreach (XmlNode node in doc.DocumentElement.ChildNodes)
            {
                Korisnik k = new Korisnik();
                XmlNode node1 = node.FirstChild;
                if (!recnik.ContainsKey(node1.InnerText))
                {
                    k.Korisnicko_ime = node1.InnerText;
                    node1 = node1.NextSibling;
                    k.Lozinka = node1.InnerText;
                    node1 = node1.NextSibling;
                    k.Ime = node1.InnerText;
                    node1 = node1.NextSibling;
                    k.Prezime = node1.InnerText;
                    node1 = node1.NextSibling;
                    if (node1.InnerText == "Man")
                    {
                        k.Pol = Pol.Musko;
                    }
                    else
                    {
                        k.Pol = Pol.Zensko;
                    }
                    node1 = node1.NextSibling;
                    if (node1.InnerText == "visitor")
                    {
                        k.Uloga = Uloga.Gost;
                        k.Rezervacije = new List<int>();
                    }
                    else
                    {
                        k.Uloga = Uloga.Domacin;
                        k.Apartmani = new List<int>();
                        k.Iznajmljeni = new List<int>();
                    }
                    recnik.Add(k.Korisnicko_ime, k);

                }
            }

        }
        catch (Exception)
        {
        }
        return recnik;
    }
}
