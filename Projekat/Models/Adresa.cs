﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class Adresa
{
    public string Ulica { get; set; }
    public string Mesto { get; set; }
    public int Posta { get; set; }
    public Adresa()
    {

    }
    public string StringInformacija()
    {
        return Ulica + "\n" + Mesto + " " + Posta + "\n";
    }
}
