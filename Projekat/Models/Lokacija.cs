﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class Lokacija
{
    public float Sirina { get; set; }
    public float Duzina { get; set; }

    public Adresa adresa;
    public string StringInformacija()
    {
        return adresa.StringInformacija() + Sirina + ", " + Duzina;
    }
    public Lokacija()
    {

    }
}
