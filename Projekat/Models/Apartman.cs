﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public enum Tip { Apartman, Soba };
public enum Status { Aktivan, Neaktivan };
public class Apartman
{
    public Tip tip { get; set; }//
    public int Broj_soba { get; set; }//
    public int Broj_gostiju { get; set; }//
    public Lokacija lokacija { get; set; }
    public List<int> komentari;
    public List<DateTime> Datumi_za_izdavanje;//
    public List<DateTime> Dostupnost_po_datumima;
    public string Domacin;
    public List<string> Slike;//
    public int Identifikacija { get; set; }
    public float Cena_po_noci { get; set; }//
    public TimeSpan Vreme_za_prijavu { get; set; }//
    public TimeSpan Vreme_za_odjavu { get; set; }//
    public Status status { get; set; }
    public Korisnik korisnik { get; set; }
    public bool Obrisan { get; set; }
    public List<Amenities> Sadrzaj;
    public List<int> Rezervacija;
    public Apartman()
    {

    }
}
