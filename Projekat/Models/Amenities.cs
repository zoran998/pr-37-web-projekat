﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class Amenities
{
    public int Id { get; set; }
    public string Naziv { get; set; }
    public string Opis { get; set; }
    public Amenities(string naziv, int id)
    {
        Id = id;
        Naziv = naziv;
    }
    public Amenities()
    {

    }
}
